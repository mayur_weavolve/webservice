<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

// Route::get('/home', 'HomeController@adminHome')->name('home');
Route::group(['middleware' => 'auth'], function(){
    Route::get('welcome','HomeController@index')->name('welcome');
    Route::get('category/list','HomeController@category')->name('category');
    Route::get('welcome/delete/{id}', 'HomeController@delete_category')->name('delete_category');
    Route::get('subcategory/list','HomeController@subcategory_list')->name('subcategory');
    Route::get('subcategory/list/delete/{id}','HomeController@delete_subcategory')->name('delete_subcategory');

    Route::get('adverts','AdvertController@index')->name('adverts');
    Route::get('adverts/block-requests','AdvertController@block_requests')->name('block_requests');
    Route::get('adverts/block-advert/{id}','AdvertController@block_advert')->name('block_advert');
    Route::get('adverts/detail/{id}','AdvertController@show')->name('advert_detail');

     Route::get('responders/register','ResponderController@responder_register')->name('responder_register');
     Route::get('responders/list','ResponderController@responder_list')->name('responder_list');
     Route::get('responder/search', 'ResponderController@search')->name('responder_search');
     Route::get('responder/detail/{id}','ResponderController@responder_detail')->name('responder_detail');
     Route::get('responders/search/{any}','ResponderController@responder_search')->name('responder_search_list');
     Route::get('responders/register/search/{any}','ResponderController@responder_register_search')->name('responder_register_search');
     Route::get('responders/register/status/{id}/{status}','ResponderController@responder_status')->name('responder_status');

     Route::get('tickets/list','SupportController@ticket_list')->name('ticket_list');
     Route::get('tickets/dispute','SupportController@ticket_dispute')->name('ticket_dispute');
     Route::get('tickets/not_respond','SupportController@user_not_respond')->name('user_not_respond');
     Route::get('tickets/close/{id}','SupportController@ticket_close')->name('ticket_close');
    Route::get('users','UserController@index')->name('users');
    Route::post('users/broadcast-notification','UserController@broadcast_notification')->name('notify-users');

    Route::get('/settings', 'SettingController@index')->name('settings');
    Route::post('/settings', 'SettingController@store')->name('settings.store');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
