<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'Api\UserController@login');
Route::post('social/login', 'Api\SocialController@social_login');
Route::post('register', 'Api\UserController@register');
Route::post('category/add', 'Api\CategoryController@category_add');
Route::post('category/list', 'Api\CategoryController@category_list');
Route::post('subcategory/list', 'Api\CategoryController@subcategory_list');
Route::post('subcategory/add', 'Api\CategoryController@subcategory_add');
Route::post('country/list', 'Api\CountryController@country');
Route::post('state/list', 'Api\CountryController@state');
Route::post('image/upload', 'Api\ImageController@image_upload');
Route::post('reach/option/list', 'Api\ReachOptionController@list');




//Route::post('ticket/status','Api\SupportController@update_ticket_status');

Route::post('forgot-password', 'Api\PasswordResetController@create');
Route::group(['middleware' => 'auth:api'], function(){
    Route::post('change-password', 'Api\UserController@change_password');
    Route::get('profile/get', 'Api\UserController@get_profile');
    Route::post('register/responder','Api\ResponderController@register_responder');

    Route::post('profile/edit', 'Api\UserController@edit_profile');
    Route::post('profile/photo/edit', 'Api\UserController@profile_pic_edit');

    Route::post('contact/add', 'Api\EmergencyContactController@add');
    Route::post('contact/delete', 'Api\EmergencyContactController@delete');

    Route::post('location/add', 'Api\UserController@add_location');

    Route::post('witness', 'Api\UserController@witness');

    Route::post('sos', 'Api\UserController@sos');

    Route::post('advert/list', 'Api\AdvertController@list');
    Route::post('advert/search', 'Api\AdvertController@search');
    Route::post('advert/add', 'Api\AdvertController@add');
    Route::post('advert/update', 'Api\AdvertController@update');

    Route::post('advert/myadvert', 'Api\AdvertController@myadvert');
    Route::post('advert/update/status', 'Api\AdvertController@advert_update_status');
    Route::post('advert/delete', 'Api\AdvertController@delete');
    Route::post('advert/block-request', 'Api\AdvertController@advert_block_request');

    Route::post('user/category/list', 'Api\CategoryController@user_category');
    Route::post('user/category/add', 'Api\CategoryController@add_user_category');
    Route::post('user/category/update', 'Api\CategoryController@update_user_category');

    Route::post('user/emergency/list', 'Api\EmergencyContactController@list');

    Route::post('payment/add', 'Api\PaymentController@payment_add');



    // Phase 2

    Route::post('request/generate','Api\RequestController@generate_request');
    Route::get('request/responder/list/{id}','Api\RequestController@get_responders');
    Route::get('requests/list/{id}','Api\ResponderController@get_requests');
    Route::post('reject/request','Api\RequestLeadController@reject_request');
    Route::post('accept/request','Api\RequestLeadController@accept_request');
    Route::post('ticket/update/status','Api\SupportController@update_status');
    Route::post('responder/update/status','Api\RequestLeadController@update_status');
  
   
    Route::get('responder/tickets','Api\UserController@getResponderTickets');
    Route::get('user/tickets','Api\UserController@getUserTickets');

    Route::post('ticket/close','Api\UserController@ticketClose');
    Route::post('ticket/complete','Api\UserController@ticketCompelete');
    Route::post('ticket/dispute','Api\UserController@ticketDispute');

    


});
