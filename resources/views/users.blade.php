@extends('layouts.admin')

@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            User List
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                <!-- <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                            <span>
                                <i class="la la-plus"></i>
                                <span>New Category</span>
                            </span>
                        </a>
                    </li>
                    <li class="m-portlet__nav-item"></li>
                    <li class="m-portlet__nav-item">

                </ul> -->
            </div>
        </div>
        <div class="m-portlet__body">
            <!-- <div class = "row">
                <div class = "col-md-12">
                    <form action = "" method = "get">
                    <div class="row">
                        <div class = "col-md-10 text-right">
                            <input type="search" name="search"class="form-control" placeholder="Search"><br>
                        </div>
                        <div class="col-md-2 text-right">
                                <button type = "submit" class ="btn btn-primary">Search</button><br>
                        </div>
                    </div>
                    </form>
                </div>
            </div> -->

            <form name="frmUsers" id="frmUsers" method="post">
                <div class="row pb-4">
                    <div class="col-md-6 col-12">
                        {!! $users->links() !!}
                    </div>
                    <div class="col-md-6 col-12 text-right">
                        <a href="javascript:void(0);" class="btn btn-brand" id="broadCastBtn" data-toggle="modal" data-target="#broadCastModal">Broadcast Notification</a>
                    </div>
                </div>
                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                        <tr>
                            <th width="5%" class="text-center">#</th>
                            <th width="25%">First Name</th>
                            <th width="25%">Last Name</th>
                            <th width="25%">Email</th>
                            <th width="20%">Mobile</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($users) == 0)
                    <tr><td colspan="8"  style="text-align:center;">No record found</td>
                    @endif
                    <?php
                    foreach ($users as $user) {?>
                    <tr>
                        <td class="text-center"><input type="checkbox" name="user_ids[]" value="{{ $user->id }}" class="user_ids"></td>
                        <td>{{ $user->first_name }}</td>
                        <td>{{ $user->last_name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->mobileno }}</td>
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>
                <div class="row pt-4">
                    <div class="col-12">
                        {!! $users->links() !!}
                    </div>
                </div>
            </form>

            <div class="clearfix">&nbsp;</div>

        </div>
    </div>
</div>

<div class="modal fade" id="broadCastModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <form name="frmBroadCastMsg" id="frmBroadCastMsg" method="post" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Broadcast Notification</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="msg_row"></div>
                    <div class="row">
                        <div class="col-12">
                            <label>Send To:</label>
                            <div class="form-group">
                                <select class="form-control" name="users_selection">
                                    <option value="1">Selected Users</option>
                                    <option value="2">All Users</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label>Title</label>
                            <div class="form-group">
                                <input type="text" class="form-control" name="title">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label>Subject</label>
                            <div class="form-group">
                                <textarea class="form-control" name="subject" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label>Flyer Image</label>
                            <div class="form-group">
                                <input type="file" name="flyer_image" id="flyer_image">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="send" class="btn btn-brand">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection

@section('custom_scripts')
    <script>
        var notifyUrl = '{{ route('notify-users') }}';
    </script>
    <script src="{{ asset('assets/js/users.js') }}"></script>
@endsection
