<div class="form-group {{ $errors->has($field['name']) ? ' has-error' : '' }}">
    <label for="{{ $field['name'] }}"><b>{{ $field['label'] }}</b></label>
    <input type="{{ $field['type'] }}"
           name="{{ $field['name'] }}"
           value="{{ old($field['name'], \App\Setting::get($field['name'])) }}"
           class="form-control {{ array_get( $field, 'class') }}"
           id="{{ $field['name'] }}"
           placeholder="{{ $field['label'] }}"
           @if(isset($field['readonly'])) readonly="{{ $field['readonly'] }} @endif">

    @if ($errors->has($field['name'])) <small class="help-block text-danger"><b>{{ $errors->first($field['name']) }}</b></small> @endif
</div>
