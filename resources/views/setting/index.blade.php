@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">

                @if (session('message'))
                    <div class="alert alert-{{ (session('type') == 'success') ? 'success' : 'danger' }} alert-dismissible m--margin-bottom-30" role="alert">
                        {{ session('message') }}
                    </div>
                @endif

                <form method="post" action="{{ route('settings.store') }}" class="form-horizontal" role="form">
                    {!! csrf_field() !!}

                    @if(count(config('setting_fields', [])) )

                        @foreach(config('setting_fields') as $section => $fields)
                            <div class="card">
                                <div class="card-header">
                                    <h3>
                                        <i class="{{ array_get($fields, 'icon', 'fas fa-cogs') }}"></i>
                                        {{ $fields['title'] }}
                                    </h3>
                                </div>

                                <div class="card-body">
                                    @if(!empty($fields['desc']))
                                        <div class="row">
                                            <div class="col-md-10 col-md-offset-2 mb-3">
                                                <p class="text-muted">{{ $fields['desc'] }}</p>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-2">
                                            @foreach($fields['elements'] as $field)
                                                @includeIf('setting.fields.' . $field['type'] )
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                            </div>
                        <!-- end panel for {{ $fields['title'] }} -->
                        @endforeach

                    @endif
                    <div class="clearfix">&nbsp;</div>
                    <div class="row m-b-md">
                        <div class="col-md-12">
                            <button class="btn-primary btn">Save Settings</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
