@extends('layouts.admin')

@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Advert Detail
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			<!-- <ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
						    <span>New Category</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item">

			</ul> -->
	    </div>
	</div>
	<div class="m-portlet__body">
		<!-- <div class = "row">
			<div class = "col-md-12">
				<form action = "" method = "get">
				<div class="row">
					<div class = "col-md-10 text-right">
						<input type="search" name="search"class="form-control" placeholder="Search"><br>
					</div>
					<div class="col-md-2 text-right">
							<button type = "submit" class ="btn btn-primary">Search</button><br>
					</div>
				</div>
				</form>
			</div>
		</div> -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<tbody>
            @if($advert)
                <tr>
                    <td><b>Name:</b></td>
                    <td>{{ $advert->name }}</td>
                </tr>
                <tr>
                    <td><b>Category:</b></td>
                    <td>{{ $advert->category->name }}</td>
                </tr>
                <tr>
                    <td><b>Sub Category:</b></td>
                    <td>{{ $advert->subcategory->name }}</td>
                </tr>
                <tr>
                    <td><b>Price:</b></td>
                    <td>{{ $advert->price }}</td>
                </tr>
                <tr>
                    <td><b>Image:</b></td>
                    <td>
                        @if($advert->photo)
                            @php( $advert->photo = Config::get('constants.image').$advert->photo);
                            <img src="{{ asset($advert->photo) }}" width="100" height="100" />
                        @endif
                    </td>
                </tr>
                <tr>
                    <td><b>Flayer Image:</b></td>
                    <td>
                        @if($advert->flayer_photo)
                            @php( $advert->flayer_photo = Config::get('constants.image').$advert->flayer_photo);
                            <img src="{{ asset($advert->flayer_photo) }}" width="100" height="100" />
                        @endif
                    </td>
                </tr>
                <tr>
                    <td><b>Description:</b></td>
                    <td>{{ $advert->description }}</td>
                </tr>
                <tr>
                    <td><b>Mobile:</b></td>
                    <td>{{ $advert->mobileno }}</td>
                </tr>
                <tr>
                    <td><b>Address:</b></td>
                    <td>{{ $advert->address }}</td>
                </tr>
                <tr>
                    <td><b>Latitude:</b></td>
                    <td>{{ $advert->latitude }}</td>
                </tr>
                <tr>
                    <td><b>Longitude:</b></td>
                    <td>{{ $advert->longitude }}</td>
                </tr>
                <tr>
                    <td><b>Reach:</b></td>
                    <td>
                        @if($advert->reach_option)
                            {{ $advert->reach_option->reach }}, {{ $advert->reach_option->display }}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td><b>Country:</b></td>
                    <td>{{ (isset($advert->country)) ?? $advert->country->country_name }}</td>
                </tr>
                <tr>
                    <td><b>State:</b></td>
                    <td>{{ (isset($advert->state)) ?? $advert->state->state_name }}</td>
                </tr>
                <tr>
                    <td><b>Expiry Date:</b></td>
                    <td>{{ $advert->expiry_date }}</td>
                </tr>
                <tr>
                    <td><b>Action</b></td>
                    @if($advert->is_blocked > 0)
                        <td>
                            <a href="{{ route('block_advert',[$advert->id])}}" title="Unblock">
                        <span style="width: 110px;">
                            <span class="m-badge  m-badge--danger m-badge--wide">Unblock</span>
                        </span>
                            </a>
                        </td>
                    @else
                        <td>
                            <a href="{{ route('block_advert',[$advert->id])}}" title="Block Advert">
                        <span style="width: 110px;">
                            <span class="m-badge  m-badge--success m-badge--wide">Block Advert</span></span>
                            </a>
                        </td>
                    @endif
                </tr>
             @endif
			</tbody>
		</table>
	</div>
</div>
</div>
</div>
</div>
@endsection
