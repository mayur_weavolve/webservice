@extends('layouts.admin')

@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Advert List
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			<!-- <ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
						    <span>New Category</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item">

			</ul> -->
	    </div>
	</div>
	<div class="m-portlet__body">
		<!-- <div class = "row">
			<div class = "col-md-12">
				<form action = "" method = "get">
				<div class="row">
					<div class = "col-md-10 text-right">
						<input type="search" name="search"class="form-control" placeholder="Search"><br>
					</div>
					<div class="col-md-2 text-right">
							<button type = "submit" class ="btn btn-primary">Search</button><br>
					</div>
				</div>
				</form>
			</div>
		</div> -->

        {!! $adverts->links() !!}

		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<thead>
				<tr>
					<!-- <th> ID</th> -->
					<th>Name</th>
                    <th>Category</th>
                    <th>Sub Category</th>
                    <th>Price</th>
                    <th>Image</th>
                    <th>Flayer Image</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			@if(count($adverts) == 0)
			<tr><td colspan="8"  style="text-align:center;">No record found</td>
			@endif
			<?php
            foreach ($adverts as $advert) {
                $image = $flayer_image = '';
                if($advert->photo)
                {
                    $advert->photo = Config::get('constants.image').$advert->photo;
                }
                if($advert->flayer_photo)
                {
                    $advert->flayer_photo = Config::get('constants.image').$advert->flayer_photo;
                }
			    ?>
			<tr>
                <td><a href="{{ route('advert_detail', $advert->id) }}">{{ $advert->name}}</a></td>
                <td>{{ $advert->category->name}}</td>
                <td>{{ $advert->subcategory->name}}</td>
                <td>{{ $advert->price}}</td>
                <td>
                    @if($advert->photo)
                        <img src="{{ asset($advert->photo) }}" width="80" height="80" />
                    @endif
                </td>
                <td>
                    @if($advert->flayer_photo)
                        <img src="{{ asset($advert->flayer_photo) }}" width="80" height="80" />
                    @endif
                </td>
				@if($advert->is_blocked > 0)
                    <td>
                        <a href="{{ route('block_advert',[$advert->id])}}" title="Unblock">
                        <span style="width: 110px;">
                            <span class="m-badge  m-badge--danger m-badge--wide">Unblock</span>
                        </span>
                        </a>
                    </td>
				@else
                    <td>
                        <a href="{{ route('block_advert',[$advert->id])}}" title="Block Advert">
                        <span style="width: 110px;">
                            <span class="m-badge  m-badge--success m-badge--wide">Block Advert</span></span>
                        </a>
                    </td>
				@endif

			</tr>
			<?php } ?>
			</tbody>
		</table>

        {!! $adverts->links() !!}

	</div>
</div>
</div>
</div>
</div>
@endsection
