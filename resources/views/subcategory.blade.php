@extends('layouts.admin')

@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						SubCategory List
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			<!-- <ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
						    <span>New Category</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item">

			</ul> -->
	    </div>
	</div>
	<div class="m-portlet__body">
		<!-- <div class = "row">
			<div class = "col-md-12">
				<form action = "" method = "get">
				<div class="row">
					<div class = "col-md-10 text-right">
						<input type="search" name="search"class="form-control" placeholder="Search"><br>
					</div>
					<div class="col-md-2 text-right">
							<button type = "submit" class ="btn btn-primary">Search</button><br>
					</div>
				</div>
				</form>
			</div>
		</div> -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<thead>
				<tr>
					<!-- <th> ID</th> -->
					<th>Category Name</th>
					<th>SubCategory Name</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
			@if(count($subcategory) == 0)
			<tr><td colspan="8"  style="text-align:center;">No record found</td>
			@endif
			<?php foreach ($subcategory as $category) { ?>
			<tr>
				<!-- <td> <?php echo $category->id ;?> </td> -->
				<td>{{ $category->category->name}}</td>
				<td>{{ $category->name}}</td>
				@if($category->status == 'active')
				<td><a href="{{ route('delete_subcategory',[$category->id])}}" title="Edit">
                <span style="width: 110px;"><span class="m-badge  m-badge--success m-badge--wide">Active</span></span></a></td>
				@else
				<td><a href="{{ route('delete_subcategory',[$category->id])}}" title="Edit">
                <span style="width: 110px;"><span class="m-badge  m-badge--danger m-badge--wide">Inactive</span></span><a></td>
				@endif
				
				</tr>
			<?php } ?>
			</tbody>

		</table>
	</div>
</div>
</div>
</div>
</div>
@endsection
