@extends('layouts.admin')

@section('content')

<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            responder Detail
                        </h3>
                    </div>

                </div>

                <div class="m-portlet__head-tools">

                <a href="{{ url()->previous() }}">
                <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" fill="#716aca" class="bi bi-arrow-left-square-fill" viewBox="0 0 16 16">
                    <path d="M16 14a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12zm-4.5-6.5H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5a.5.5 0 0 0 0-1z"/>
                </svg>
                </a>
			</div>
        </div>
        <div class="m-portlet__body">

                <div class="table-responsive">
                <table class="table table-striped- table-bordered table-hover twoColumn" id="m_table_1">
                    <tbody>
                    <tr>   <th>User Id</th> <td>{{ $responder->user_id }}</td></tr>
                    <tr>  <th>First Name </th>  <td> {{ $responder->user["first_name"] }}</td> </tr>
                    <tr> <th>Last Name</th>  <td>{{ $responder->user["last_name"] }}</td> </tr>
                    <tr> <th>Responder Type</th>   <td>{{ $responder->responder_type }}</td> </tr>
                    <tr>  <th>Mobile Number</th>  <td>{{ $responder->user["mobileno"] }}</td> </tr>
                    <tr>  <th>Email Id</th>   <td>{{ $responder->user["email"] }}</td> </tr>
                    <tr>  <th>Uniform Photo</th>  <td>   <img style="max-width:20%;" src="{{asset('/images/uniform/').'/'.$responder->uniform_photo }}">   </td> </tr>
                    <tr>  <th>Employer Name</th>   <td>{{ $responder->employer_name }}</td> </tr>
                    @if($responder->responder_type == "patrol car")
                    <tr>  <th>Vehicle Number</th>  <td>{{ $responder->vehicle_number }}</td>    </tr>
                    <tr>  <th>Vehicle Photo</th>    <td> <img style="max-width:20%;" src="{{asset('/images/vehicle/').'/'.$responder->vehicle_photo }}"></td> </tr>
                    @endif
                    <tr>  <th>Status</th>    <td>{{ $responder->status }}</td> </tr>
                    </tbody>
                </table>
                </div>
            <div class="clearfix">&nbsp;</div>

        </div>
    </div>
</div>


@endsection

@section('custom_scripts')
    <script>
        var notifyUrl = '{{ route('notify-users') }}';
    </script>
    <script src="{{ asset('assets/js/users.js') }}"></script>
@endsection
