@extends('layouts.admin')

@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                          Ticket List
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                <a href="{{ url()->previous() }}">
                <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" fill="#716aca" class="bi bi-arrow-left-square-fill" viewBox="0 0 16 16">
                    <path d="M16 14a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12zm-4.5-6.5H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5a.5.5 0 0 0 0-1z"/>
                </svg>
                </a>
            </div>
        </div>
        <div class="m-portlet__body">
            <!-- <div class = "row">
                <div class = "col-md-12">
                    <form action = "" method = "get">
                    <div class="row">
                        <div class = "col-md-10 text-right">
                            <input type="search" name="search"class="form-control" placeholder="Search"><br>
                        </div>
                        <div class="col-md-2 text-right">
                                <button type = "submit" class ="btn btn-primary">Search</button><br>
                        </div>
                    </div>
                    </form>
                </div>
            </div> -->

            <form name="frmUsers" id="frmUsers" method="post">
                <div class="row pb-4">
                    <div class="col-md-6 col-12">
                        {!! $tickets->links() !!}
                    </div>
                    <!-- <div class="col-md-6 col-12 text-right">
                        <a href="javascript:void(0);" class="btn btn-brand" id="broadCastBtn" data-toggle="modal" data-target="#broadCastModal">Broadcast Notification</a>
                    </div> -->
                </div>
                <div class="table-responsive">
                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                        <tr class="text-center">
                            <th>#</th>
                            <th>Request Id </th>
                            <th>User Name </th>
                            <th>User MobileNo </th>
                            <th>Responder Name </th>
                            <th>Responder MobileNo </th>
                            <th>Subject</th>
                            <th>Description</th>
                            <th>Status</th> 
                            <th>Date</th> 
                            <th class="text-center" colspan="2">action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($tickets) == 0)
                    <tr><td colspan="8"  style="text-align:center;">No record found</td>
                    @endif
                    <?php
                    foreach ($tickets as $ticket) {?>
                    <tr class="text-center">
                        <td class="text-center"><input type="checkbox" name="responder_ids[]" value="{{ $ticket->id }}" class="user_ids"></td>
                        <td>{{ $ticket->request_id }}</td>
                        <td>{{ $ticket->requestInfo->user->first_name }}</td>
                        <td>{{ $ticket->requestInfo->user->mobileno }}</td>
                         <td>{{ $ticket->responderInfo->first_name }}</td>
                         <td>{{ $ticket->responderInfo->mobileno }}</td>
                        <td>{{ $ticket->subject }}</td>
                        <td>{{ $ticket->discription }}</td>
                         <td>{{ $ticket->status }}</td>
                         <td>{{date_format($ticket->created_at,"d/m/Y H:i:s") }}</td>   
                
                         
                          <td> 
                          <a href="{{ route('ticket_close',['id'=>$ticket->id]) }}" class="btn btn-danger">Close</a>
                         </td>
                        
 
                   
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>
                </div>
                <div class="row pt-4">
                    <div class="col-12">
                        {!! $tickets->links() !!}
                    </div>
                </div>
            </form>

            <div class="clearfix">&nbsp;</div>

        </div>
    </div>
</div>

<div class="modal fade" id="broadCastModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <form name="frmBroadCastMsg" id="frmBroadCastMsg" method="post" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Broadcast Notification</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="msg_row"></div>
                    <div class="row">
                        <div class="col-12">
                            <label>Send To:</label>
                            <div class="form-group">
                                <select class="form-control" name="users_selection">
                                    <option value="1">Selected Users</option>
                                    <option value="2">All Users</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label>Title</label>
                            <div class="form-group">
                                <input type="text" class="form-control" name="title">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label>Subject</label>
                            <div class="form-group">
                                <textarea class="form-control" name="subject" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label>Flyer Image</label>
                            <div class="form-group">
                                <input type="file" name="flyer_image" id="flyer_image">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="send" class="btn btn-brand">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection

@section('custom_scripts')
    <script>
        var notifyUrl = '{{ route('notify-users') }}';
    </script>
    <script src="{{ asset('assets/js/users.js') }}"></script>
@endsection
