<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResponderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('responders', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->string('responder_type')->nullable();
            $table->string('uniform_photo')->nullable();
            $table->string('employer_name')->nullable();
            $table->string('vehicle_number')->nullable();
            $table->string('vehicle_photo')->nullable();
            $table->string('status')->nullable();
            $table->string('rejection_reason')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('responders');
    }
}
