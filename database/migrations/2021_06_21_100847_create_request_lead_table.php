<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestLeadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
        {
            Schema::create('request_leads', function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('request_id');
                $table->foreign('request_id')->references('id')->on('sos_requests');
                $table->unsignedBigInteger('user_id');
                $table->foreign('user_id')->references('id')->on('users');
                
                $table->double('latitude')->nullable();
                $table->double('longitude')->nullable();
                $table->string('status')->nullable();
                $table->timestamps();
            });
        }
    
        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('request_leads');
        }
}
