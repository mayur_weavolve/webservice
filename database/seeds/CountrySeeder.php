<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('country')->insert([
            'country_code' => '',
            'country_name' => 'usa',
        ]);
        DB::table('country')->insert([
            'country_code' => '',
            'country_name' => 'kenya',
        ]);
    }
}
