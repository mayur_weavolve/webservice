<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Alabama',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Alaska',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Arizona',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Arkansas',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'California',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Colorado',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Connecticut',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Delaware',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Florida',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Georgia',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Hawaii',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Idaho',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Illinois',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Indiana',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Iowa',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Kansas',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Kentucky',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Louisiana',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Maine',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Maryland',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Massachusetts',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Michigan',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Minnesota',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Mississippi',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Missouri',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Montana',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Nebraska',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Nevada',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'New Hampshire',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'New Jersey',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'New Mexico',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'New York',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'North Carolina',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'North Dakota',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Ohio',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Oklahoma',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Oregon',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Pennsylvania',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Rhode Island',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'South Carolina',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'South Dakota',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Tennessee',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Texas',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Utah',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Vermont',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Virginia',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Washington',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'West Virginia',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Wisconsin',
        ]);
        DB::table('states')->insert([
            'country_id' => 1,
            'state_code' => '',
            'state_name' => 'Wyoming',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Mombasa',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Kwale',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Kilifi',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Tana River',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Lamu',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Taita-Taveta',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Garissa',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Wajir',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Mandera',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Marsabit',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Isiolo',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Meru',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Tharaka',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Embu',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Kitui',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Machakos',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Makueni',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Nyandarua',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Nyeri',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Kirinyaga',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => "Murang'a",
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Kiambu',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Turkana',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'West Pokot',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Samburu',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Trans-Nzoia',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Uasin Gishu',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Elgeyo-Marakwet',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Nandi',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Baringo',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Laikipia',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Nakuru',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Narok',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Kajiado',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Kericho',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Bomet',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Kakamega',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Vihiga',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Bungoma',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Busia',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Siaya',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Kisumu',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Homa Bay',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Migori',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Kisii',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Nyamira',
        ]);
        DB::table('states')->insert([
            'country_id' => 2,
            'state_code' => '',
            'state_name' => 'Nairobi',
        ]);
        
    }
}
