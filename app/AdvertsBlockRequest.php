<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdvertsBlockRequest extends Model
{
    //use SoftDeletes;
    protected $fillable = ['user_id','advert_id','reason'];

    public function advert(){
        return $this->hasOne('App\Advert','id','advert_id');
    }

    public function user(){
        return $this->hasOne('App\User','id','user_id');
    }
}
