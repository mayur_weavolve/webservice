<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Responder extends Model
{
    protected $fillable = [
        'user_id','responder_type','uniform_photo','employer_name','vehicle_number','vehical_photo','status','rejection_reson',
    ];
    public function user()
    {
        return $this->hasOne("App\User",'id','user_id');
    }
}
