<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCategory extends Model
{
    protected $fillable = [
        'user_id','category_id','subcategory_id',
    ];
    public function advert()
    {
      return $this->hasMany("App\Advert",'category_id','category_id');
    }
}
