<?php

namespace App\Console\Commands;

use App\Advert;
use App\AdvertsBlockRequest;
use Carbon\Carbon;
use Illuminate\Console\Command;

class DeleteBlockedAdverts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'adverts:delete-blocked';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command is use to delete blocked adverts after one month.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Advert::where('is_blocked',1)->where('delete_date','<',Carbon::now()->format('Y-m-d'))->each(function ($advert) {
            $advert->adverts_block_requests()->delete();
            $advert->delete();
        });
        $this->info('All blocked adverts has been deleted');
    }
}
