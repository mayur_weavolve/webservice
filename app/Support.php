<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Support extends Model
{
    protected $fillable = [ 
        'request_id',
        'responder_id',
        'subject',
        'discription',
        'status',
    ];

    public function requestInfo(){

        return $this->hasOne('App\SosRequest','id','request_id');
    }

    public function responderInfo()
    {
        return $this->hasOne("App\User",'id','responder_id');
    }

    public function leadRequestInfo()
    {
        return $this->hasOne("App\Request_lead",'request_id','request_id');
         
    }
}
