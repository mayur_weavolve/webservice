<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Request_lead extends Model
{
    protected $filllable = [
       'user_id','latitude','longitude','status',
    ];
    public function responder(){
        return $this->hasOne("App\User",'id','user_id');
    }
}
