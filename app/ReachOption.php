<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReachOption extends Model
{
    protected $fillable = [
        'option','reach','price',
    ];
}
