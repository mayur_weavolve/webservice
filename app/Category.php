<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name','status',
    ];
    public function subcategory()
    {
      return $this->hasMany("App\SubCategory",'category_id','id')->where('status',"=",'active');
    }
}
