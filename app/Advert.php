<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Advert extends Model
{
    //use SoftDeletes;
    protected $fillable = [
        'user_id','category_id','subcategory_id','description','price','name','address','mobileno',
        'latitude','longitude','reach_id','photo','flayer_photo','is_my_advert','country_id',
        'state_id','status','expiry_date','web_url',
    ];

    public function category()
    {
      return $this->hasOne("App\Category",'id','category_id');
    }
    public function subcategory()
    {
      return $this->hasOne("App\SubCategory",'id','subcategory_id');
    }
    public function user(){
        return $this->hasOne("App\User","id","user_id");
    }
    public function user_category()
    {
      return $this->hasMany("App\UserCategory",'user_id','user_id');
    }
    public function reach_option()
    {
        return $this->hasOne("App\ReachOption",'id','reach_id');
    }
    public function country()
    {
        return $this->hasOne("App\Country",'id','country_id');
    }
    public function state()
    {
        return $this->hasOne("App\State",'id','state_id');
    }

    public function adverts_block_requests()
    {
        return $this->hasMany('App\AdvertsBlockRequest');
    }
}
