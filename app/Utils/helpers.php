<?php
if (! function_exists('setting')) {

    function setting($key, $default = null)
    {
        if (is_null($key)) {
            return new \App\Setting();
        }

        if (is_array($key)) {
            return \App\Setting::set($key[0], $key[1]);
        }

        $value = \App\Setting::get($key);

        return is_null($value) ? value($default) : $value;
    }
}

if(! function_exists('sendIOSNotification')){
    function sendIOSNotification($env, $notify_data, $device_ids){
        $gateway_url = '';
        if($env == 'test') {
            $gateway_url = APPLE_SERVER_SANDBOX_URL;
        }
        else {
            $gateway_url = APPLE_SERVER_URL;
        }

        $certificate = public_path('SimbaSOSPush.pem');
        $passphrase = '1234';
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $certificate);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        //stream_context_set_option( $ctx , 'ssl', 'verify_peer', false);
        $fp = stream_socket_client($gateway_url, $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

        if($err || !$fp) {
            die("Failed to connect: $err $errstr" . PHP_EOL);
        }

        foreach($device_ids as $device_id) {
            // Create the payload body
            $body['aps'] = [
                'alert' => [
                    'title' => $notify_data['title'] ?? 'Simba SOS Notification',
                    'body' => $notify_data['message']
                ],
                'badge' => 1,
                'sound' => 'default',
                'category' => '',
                'mutable-content' => true
            ];

            //$body += $extra_data;

            $payload = json_encode($body);

            $message = chr(0) . pack('n', 32) . strtr(rtrim(base64_encode(pack('H*', sprintf('%u', CRC32($device_id)))), '='), '+/', '-_') . pack('n', strlen($payload)) . $payload;
            $result = fwrite($fp, $message, strlen($message));
            if(!$result){
                die('Oops! Something went wrong and notification wasn\'t sent');
            }
        }

        fclose($fp);
    }
}
