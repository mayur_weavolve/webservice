<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SosRequest extends Model
{
    protected $filllable = [
        'user_id','latitude','longitude','status',
    ];

    public function user(){
        return $this->hasOne('App\User','id','user_id');
    }

    public function support(){
        return $this->hasOne('App\Support','request_id','id');
    }
}
