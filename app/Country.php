<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = [
        'country_code','country_name',
    ];
    protected $table = 'country';

    public function state()
    {
      return $this->hasMany("App\State",'country_id','id');
    }

}
