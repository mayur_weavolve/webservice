<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Advert;
use App\AdvertsBlockRequest;

class AdvertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adverts = Advert::orderBy('created_at', 'DESC')->paginate(10);
        return view('adverts', compact('adverts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $advert = Advert::find($id);
        return view("advert_detail", compact('advert'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function block_requests(){
        $block_requests = AdvertsBlockRequest::orderBy('created_at', 'DESC')->paginate(10);
        return view('advert_block_requests',compact('block_requests'));
    }

    public function block_advert($id){
        $advert = Advert::find($id);
        if($advert->is_blocked == '0'){
            $advert->is_blocked = '1';
            $advert->delete_date = Carbon::now()->addMonth()->format('Y-m-d');
            $advert->save();
        }
        else{
            $advert->is_blocked = '0';
            $advert->delete_date = null;
            $advert->save();
        }
        return redirect()->back();
    }
}
