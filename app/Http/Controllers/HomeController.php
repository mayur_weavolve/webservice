<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\Category;
use App\SubCategory;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('welcome');
    }
    public function category()
    {
        $user = Auth::user();
        $category = Category::get();
        return view('category')->with('category',$category);    
    }
    // public function adminhome()
    // {
    //     return view('home');
    // }
    public function subcategory_list()
    {
        $list = SubCategory::select('sub_categories.*')->with('category')
                ->join('categories','categories.id','sub_categories.category_id')
                ->where('categories.status','active')
                ->get();
        return view('subcategory')->with('subcategory',$list);
    }
    public function delete_category($id)
    {
        $delete=Category::find($id);
        if($delete->status == 'active')
        {
            $delete->status = 'inactive';
            $delete->save();
        }
        else{
            $delete->status = 'active';
            $delete->save();
        }
        
        return redirect('category/list');
    }
    public function delete_subcategory($id)
    {
        $delete=SubCategory::find($id);
        if($delete->status == 'active')
        {
            $delete->status = 'inactive';
            $delete->save();
        }
        else{
            $delete->status = 'active';
            $delete->save();
        }
        
        return redirect('subcategory/list');
    }
    
}
