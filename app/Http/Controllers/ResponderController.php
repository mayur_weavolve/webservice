<?php

namespace App\Http\Controllers;
use App\Responder;
use App\User;
use Illuminate\Http\Request;

class ResponderController extends Controller
{
    public function responder_register(){
        $responders = Responder::where('status','=','pending')->with('user')->paginate(10);
        return view('responders.responderList', compact('responders'))->with(['type'=>"petrol man"]);
    }
    public function responder_list(){
      $responders = Responder::where('status','!=','pending')->with('user')->paginate(10);
      return view('responders.responders', compact('responders'))->with(['type'=>"petrol man"]);
  }
    public function responder_search($any){
        $responders = Responder::with('user')
        ->where('responder_type','=',$any)
        ->where('status','!=','pending')
       ->paginate(10);
        return view('responders.responders', compact('responders'))->with(['type'=>$any]);
    }
    public function responder_register_search($any){
      $responders = Responder::with('user')
      ->where('responder_type','=',$any)
     ->where('status','=','pending')
     ->paginate(10);
      return view('responders.responderList', compact('responders'))->with(['type'=>$any]);
  }
    public function responder_detail($id){
      //  return $id;
      $responder = Responder::where('user_id',$id)->with('user')->first();
        return view('responders.responderDetail',compact('responder'));
    }
    public function responder_status($id,$status){
     $responders = Responder::find($id);
    $responders->status = $status;
    $responders->save();
    return redirect()->back();
    }
    public function search(Request $request){
     $search = $request->input('search');
     // $var="rita";
    // $responders = Responder::where('status','!=','pending')->with('user')
    // ->where('users.first_name', 'LIKE', "%{$search}%")
    //   // ->orWhere('last_name', 'LIKE', "%{$search}%")
    //   ->paginate(10);
     
   $responders = Responder::with([ "user" => function($q) use ($search){
      $q->where('users.first_name','like', '%'.$search.'%');
  }])->paginate(10);

      return view('responders.responders', compact('responders'))->with(['type'=>"petrol man"]);
  
   }
   
  
}
