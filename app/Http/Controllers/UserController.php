<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::select('id','first_name','last_name','email','mobileno')->orderBy('first_name','ASC')->paginate(10);
        return view('users', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function broadcast_notification(Request $request){
       $rules = [
           'users_selection' => 'required',
           'title' => 'required',
           'subject' => 'required'
       ];
       $customMessages = [];
       $postData = $request->all();

       if(isset($postData['users_selection']) && $postData['users_selection']==1){
           $rules['user_ids'] = 'required';
           $customMessages['user_ids.required'] = 'Please select users for notification.';
       }

        if($request->file('flyer_image')){
            $rules['flyer_image'] = 'image|mimes:jpeg,png,jpg,gif,svg';
        }

       $validator = Validator::make($request->all(), $rules, $customMessages);
       if ($validator->fails()) {
            return response()->json(['status' => false,'errors' => $validator->errors()]);
       }
       $users_selection = $postData['users_selection'];
       $title = $postData['title'];
       $subject = $postData['subject'];
        $user_ids = [];
        if(isset($postData['users_selection']) && $postData['users_selection']==1){
            $user_ids = json_decode($postData['user_ids'], true);
        }
       $query = User::select('device_token');
       if($users_selection == 1 && is_array($user_ids) && !empty($user_ids)){
           $query->whereIn('id', $user_ids);
       }
       //$query->whereIn('email',['vdev.developer2@gmail.com','j.d1@alliancetek.com']);
       $users = $query->get()->toArray();

       $userTokens = [];
       if(!empty($users)){
           foreach($users as $user){
               $userTokens[] = $user['device_token'];
           }
       }

       $image_url = '';
        if($request->file('flyer_image')){
            $flyer_image = $request->file('flyer_image');
            $image_name = md5($flyer_image->getClientOriginalName().time()).'.'.$flyer_image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/broadcast_images');
            $flyer_image->move($destinationPath, $image_name);

            $image_url = url('uploads/broadcast_images/'.$image_name);
        }

        $notify_msg = $subject;
        $notify_title = $title;

       // Android Notification
       $notification = array(
           "title" => $notify_title,
           "subject" => $notify_msg,
           "notification_type" => 2,
           'image' => $image_url,
           "body" => $notify_msg,
           "mutable-content" => 1
       );

       $result = $this->sendFCMMessage( FCM_SERVER_KEY, $notification , $userTokens);

       if($result){
           return response()->json(['status' => true,'message' => 'Push notification sent successfully!']);
       }
    }

    public function sendFCMMessage($server_key,$notification, $target){
        $url = 'https://fcm.googleapis.com/fcm/send';
        if(is_array($target)){
            $arrayToSend = array('registration_ids' => $target,
                'data' => $notification,
                'notification' => $notification,
                'priority'=>'high');
        }else{
            $arrayToSend = array('to' => $target, 'notification' => $notification,'priority'=>'high');
        }
        //header with content_type api key
        //echo json_encode($arrayToSend);exit;
        $json = json_encode($arrayToSend);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key='. $server_key;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
        //Send the request
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Oops! FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
        exit;
    }
}
