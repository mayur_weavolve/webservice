<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use Validator;
use App\User;
use App\UserCategory;

use App\EmergencyContact;

class EmergencyContactController extends Controller
{
    public function add(Request $request)
    {
        $user = Auth::user();
        $users = User::where('id',$user->id)->first();
        $user_category = UserCategory::where('user_id',$user->id)->first();
        $data = $request->all();
        if($request->get('emergency_contact')) 
        {
            foreach($data['emergency_contact'] as $contact){
                if($contact){
                    if(isset($contact['id'])){
                        $add = EmergencyContact::find($contact['id']);
                    }
                    else{
                        $add = new EmergencyContact();
                    }
                    $add->user_id = $user->id;
                    $add->name = $contact['name'];
                    $add->mobileno = $contact['mobileno'];
                    $add->status = 'active';
                    $add->is_deleted = '0';
                    $add->save(); 
                }
            }
        }
        if($users && isset($users->is_witness) && $user_category)
        {
            $user->is_first_login = '0';
            $user->save();
        }
        else{
            $user->is_first_login = '1';
            $user->save();
        }
        return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Emergency Contact Number Add Successfully....!",'data' =>$add]);
    }
    // public function edit($id,Request $request)
    // {
    //     $validator = Validator::make($request->all(), [ 
    //         'name' => 'required',  
    //         'mobileno' => 'required', 
    //     ]);
    //     if ($validator->fails()) { 
    //         return response()->json(['status'=> false,'ErrorCode' => '400' ,"message" =>"Somthing Wrong Please try again",'data' => $validator->errors()]);
    //     }
    //     $user = Auth::user();
    //     $edit = EmergencyContact::find($id);
    //     $edit->name = $request->input('name');
    //     $edit->mobileno = $request->input('mobileno');
    //     $edit->status= 'active';
    //     $edit->is_deleted= '0';
    //     $edit->save();
    //     return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Emergency Contact Number has been change Successfully....!",'data' =>$edit]);

    // }
    public function list()
    {
        $user = Auth::user();
        $list = EmergencyContact::where('user_id',$user->id)
                ->where('is_deleted','0')
                ->get();
        if($list)
        {
            return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Emergency Contact Number List....!",'data' =>$list]);
        }
        else{
            return response()->json(['status'=> false,'statusCode' => '400' ,"message" =>"No data found....!",'data' =>null]);
        }
    }
    public function delete(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();
        $delete = EmergencyContact::where('id',$data['id'])->first();
        if($delete)
        {
            $delete->is_deleted = '1';
            $delete->status = 'inactive';
            $delete->save();            
            return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Emergency Contact Number Deleted Successfully....!",'data' =>null]);
        }
        else{
            return response()->json(['status'=> false,'statusCode' => '400' ,"message" =>"No data found....!",'data' =>null]);
        }
    }
}
