<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;
use DB;

class SocialController extends Controller
{
    public function social_login(Request $request)
    {
        $data = $request->all();
        if($data['type'] == 'facebook')
        {
            $user = User::where('facebook_id',$data['facebook_id'])->first();
            if($user)
            {
                $user->device_id = $request->input('device_id');
                $user->device_token = $request->input('device_token');
                $user->is_first_login =  '0';
                $user->save();
                $success['token'] =  $user->createToken('MyApp')->accessToken;
                $success['user_id'] =  $user->id;
                $success['first_name'] =  $user->first_name;
                $success['last_name'] =  $user->last_name;
                $success['email'] =  $user->email;
                $success['mobileno'] =  $user->mobileno;
                $success['profile_photo'] =  $user->profile_photo;
                $success['type'] =  $user->type;
                $success['is_first_login'] =  $user->is_first_login;;
                $success['is_social_login'] =  $user->is_social_login;
                $success['is_password_change'] =  $user->is_password_change;
                return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Login Sucessfully",'data' =>$success]);
            }
            $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email|unique:users',
                'type' => 'required',
                'is_social_login'=>'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['status'=>false,'statusCode' => '400' ,"message" =>"Somthing Wrong Please try again",'data' => $validator->errors()]);
            }
            $data['password'] = '';
            $data['is_first_login'] = '1';
            $data['is_password_change'] ='0';
            $user = User::create($data);
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $success['user_id'] =  $user->id;
            $success['first_name'] =  $user->first_name;
            $success['last_name'] =  $user->last_name;
            $success['email'] =  $user->email;
            $success['mobileno'] =  $user->mobileno;
            $success['profile_photo'] =  $user->profile_photo;
            $success['type'] =  $user->type;
            $success['is_first_login'] =  $user->is_first_login;;
            $success['is_social_login'] =  $user->is_social_login;
            $success['is_password_change'] =  $user->is_password_change;
            return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"User Register Successfully....!",'data' =>$success]);

        }
        if($data['type'] == 'google')
        {
            $user = User::where('google_id',$data['google_id'])->first();
            if($user)
            {
                $user->device_id = $request->input('device_id');
                $user->device_token = $request->input('device_token');
                $user->is_first_login =  '0';
                $user->save();
                $success['token'] =  $user->createToken('MyApp')->accessToken;
                $success['user_id'] =  $user->id;
                $success['first_name'] =  $user->first_name;
                $success['last_name'] =  $user->last_name;
                $success['email'] =  $user->email;
                $success['mobileno'] =  $user->mobileno;
                $success['profile_photo'] =  $user->profile_photo;
                $success['type'] =  $user->type;
                $success['is_first_login'] =  $user->is_first_login;;
                $success['is_social_login'] =  $user->is_social_login;
                $success['is_password_change'] =  $user->is_password_change;
                return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Login Sucessfully....!",'data' =>$success]);
            }

            $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email|unique:users',
                'type' => 'required',
                'is_social_login'=>'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['status'=>false,'statusCode' => '400' ,"message" =>"Somthing Wrong Please try again",'data' => $validator->errors()]);
            }
            $data['password'] = '';
            $data['is_first_login'] = '1';
            $data['is_password_change'] ='0';
            $user = User::create($data);
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $success['user_id'] =  $user->id;
            $success['first_name'] =  $user->first_name;
            $success['last_name'] =  $user->last_name;
            $success['email'] =  $user->email;
            $success['mobileno'] =  $user->mobileno;
            $success['profile_photo'] =  $user->profile_photo;
            $success['type'] =  $user->type;
            $success['is_first_login'] =  $user->is_first_login;;
            $success['is_social_login'] =  $user->is_social_login;
            $success['is_password_change'] =  $user->is_password_change;
            return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"User Register Successfully....!",'data' =>$success]);
        }
        if($data['type'] == 'apple')
        {
            $user = User::where('apple_id',$data['apple_id'])->first();
            if($user)
            {
                $user->device_id = $request->input('device_id');
                $user->device_token = $request->input('device_token');
                $user->is_first_login =  '0';
                $user->save();
                $success['token'] =  $user->createToken('MyApp')->accessToken;
                $success['user_id'] =  $user->id;
                $success['first_name'] =  $user->first_name;
                $success['last_name'] =  $user->last_name;
                $success['email'] =  $user->email;
                $success['mobileno'] =  $user->mobileno;
                $success['profile_photo'] =  $user->profile_photo;
                $success['type'] =  $user->type;
                $success['is_first_login'] =  $user->is_first_login;;
                $success['is_social_login'] =  $user->is_social_login;
                $success['is_password_change'] =  $user->is_password_change;
                return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Login Sucessfully....!",'data' =>$success]);
            }

            $user = User::where('email',$data['email'])->first();
            if($user)
            {
                $user->device_id = $request->input('device_id');
                $user->device_token = $request->input('device_token');
                $user->apple_id = $request->input('apple_id');
                $user->is_first_login =  '0';
                $user->save();
                $success['token'] =  $user->createToken('MyApp')->accessToken;
                $success['user_id'] =  $user->id;
                $success['first_name'] =  $user->first_name;
                $success['last_name'] =  $user->last_name;
                $success['email'] =  $user->email;
                $success['mobileno'] =  $user->mobileno;
                $success['profile_photo'] =  $user->profile_photo;
                $success['type'] =  $user->type;
                $success['is_first_login'] =  $user->is_first_login;;
                $success['is_social_login'] =  $user->is_social_login;
                $success['is_password_change'] =  $user->is_password_change;
                return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Login Sucessfully....!",'data' =>$success]);
            }
            
            $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email|unique:users',
                'type' => 'required',
                'is_social_login'=>'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['status'=>false,'statusCode' => '400' ,"message" =>"Somthing Wrong Please try again",'data' => $validator->errors()]);
            }
            $data['password'] = '';
            $data['is_first_login'] = '1';
            $data['is_password_change'] ='0';
            $user = User::create($data);
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $success['user_id'] =  $user->id;
            $success['first_name'] =  $user->first_name;
            $success['last_name'] =  $user->last_name;
            $success['email'] =  $user->email;
            $success['mobileno'] =  $user->mobileno;
            $success['profile_photo'] =  $user->profile_photo;
            $success['type'] =  $user->type;
            $success['is_first_login'] =  $user->is_first_login;;
            $success['is_social_login'] =  $user->is_social_login;
            $success['is_password_change'] =  $user->is_password_change;
            return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"User Register Successfully....!",'data' =>$success]);
        }
    }
}
