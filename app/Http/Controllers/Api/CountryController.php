<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Country;
use App\State;

class CountryController extends Controller
{
    public function country()
    {
        $country = Country::get();
        return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Country Listing....",'data' => $country]);
    }

    public function state()
    {
        $state = Country::with('state')->get();
        return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"State Listing....",'data' => $state]);
    }
}
