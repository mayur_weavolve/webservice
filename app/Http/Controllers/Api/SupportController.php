<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Support;
use App\SosRequest;
use App\Responder;
use Validator;

class SupportController extends Controller
{
    public function support_request(Request $request){
        //  return $request;
          $validator = Validator::make($request->all(), [
              'request_id' => 'required|unique:supports',
              'responder_id' => 'required',
              'subject'=> 'required',
              'discription'=> 'required',
              'status'=> 'required',
          ]);
          if ($validator->fails()) {
              return response()->json(['status'=>false,'statusCode' => '400' ,"message" =>"Somthing Wrong Please try again",'data' => $validator->errors()]);
          }else{
            $find_req = SosRequest::find($request->get('request_id'));
            $find_responder = Responder::find($request->get('responder_id'));
            //return response()->json(['status'=>true,'statusCode' => '200' ,"req" =>$find_req,"responder"=>$find_responder]);

            if(!empty($find_req) && !empty($find_responder)){
                $support = new Support;
                $support->request_id = $request->get('request_id');
                $support->responder_id = $request->get('responder_id');
                $support->subject = $request->get('subject');
                $support->discription = $request->get('discription');
                $support->status = $request->get('status');
                $support->save();
               return response()->json(['status'=>true,'statusCode' => '200' ,"message" =>"request generated successfully"]);
    
            }
            else if($find_req == null && $find_responder != null){
                return response()->json(['status'=>true,'statusCode' => '400' ,"message" =>"request not found"]);
                }
            else if($find_responder == null && $find_req != null){
                    return response()->json(['status'=>true,'statusCode' => '400' ,"message" =>"responder not found"]);
                }
            else{
                return response()->json(['status'=>true,'statusCode' => '400' ,"message" =>"responder and request are not found"]);

            }
         
          }
      }

      public function update_status(Request $request){
        $validator = Validator::make($request->all(), [
            'request_id' => 'required|numeric',
            'status'=> 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>false,'statusCode' => '400' ,"message" =>"Somthing Wrong Please try again",'data' => $validator->errors()]);
        }
        else{
          $findId = Support::where('request_id',$request->get('request_id'))->first();
          if(!empty($findId)){
            $findId->status=$request->get('status');
            if($findId->save()){
                return response()->json(["status"=>"200","messsage"=>"status updated successfully"]);
            }
            else{
              return response()->json(["status"=>"400","messsage"=>"status not updated"]);
            }
          }
          else{
            return response()->json(["status"=>"400","messsage"=>"ticket not found"]);
 
          }
         
        }
      }
      // public function update_ticket_status(Request $request){
      //   $validator = Validator::make($request->all(), [
      //     'request_id' => 'required|numeric',
      //     'status'=> 'required',
      // ]);
      // if ($validator->fails()) {
      //     return response()->json(['status'=>false,'statusCode' => '400' ,"message" =>"Somthing Wrong Please try again",'data' => $validator->errors()]);
      // }

      // }

      
}
