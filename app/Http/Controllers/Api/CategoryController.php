<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
Use App\User;
Use App\EmergencyContact;
use App\UserCategory;
use App\Category;
use App\SubCategory;
use Validator;

class CategoryController extends Controller
{
    public function category_add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=> false,'ErrorCode' => '400' ,"message" =>"Somthing Wrong Please try again",'data' => $validator->errors()]);
        }
        $data = $request->all();
        $data['status'] = 'inactive';
        $category = Category::create($data);
        return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Category requested successfully....",'data' => $category]);

    }
    public function category_list()
    {
        $category = Category::where('status','active')->get();
        if(count($category))
        {
            return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Category Liting....",'data' => $category]);
        }
        else{
            return response()->json(['status'=> false,'statusCode' => '400' ,"message" =>"No Data Found....",'data' => null]);
        }
    }
    public function subcategory_add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'category_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=> false,'ErrorCode' => '400' ,"message" =>"Somthing Wrong Please try again",'data' => $validator->errors()]);
        }
        $data = $request->all();
        $data['status'] = 'inactive';
        $subcategory = SubCategory::create($data);
        return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"SubCategory requested successfully....",'data' => $subcategory]);

    }
    public function subcategory_list()
    {
        $category = Category::with('subcategory')->where('status','active')->get();
        // $category = Category::select('categories.*')->with('subcategory')
        //         ->join('sub_categories','sub_categories.category_id','categories.id')
        //         ->where('categories.status','active')
        //         ->where('sub_categories.status','active')
        //         ->get();
        if(count($category))
        {
            $categories = [];
            foreach ($category as $cat){
                $cat->checked = false;
                if(count($cat->subcategory)){
                    foreach ($cat->subcategory as $k => $subcat){
                        $cat->subcategory[$k]->checked = false;
                    }
                }
                $categories[] = $cat;
            }
            return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Category Liting....",'data' => $categories]);
        }
        else{
            return response()->json(['status'=> false,'statusCode' => '400' ,"message" =>"No Data Found....",'data' => null]);
        }
    }
    public function user_category()
    {
        $user = Auth::user();
        $user_category = UserCategory::where('user_id',$user->id)->get();
        //$parentCats = $user_category->pluck('category_id');
        $childCats = $user_category->pluck('subcategory_id')->toArray();
        if(count($user_category))
        {
            $userCategories = $this->getUserSelectedCats($childCats);
            return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>" User Category Liting....",'data' => $userCategories]);
        }
        else{
            return response()->json(['status'=> false,'statusCode' => '400' ,"message" =>"No Data Found....",'data' => null]);
        }
    }

    public function getUserSelectedCats($userChildCats = array()){
        $categories = Category::with('subcategory')->where('status','active')->get();
        $userCategories = [];
        foreach ($categories as $category){
            $category->checked = true;
            if(count($category->subcategory)){
                foreach ($category->subcategory as $k => $subcat){
                    if(!in_array($subcat->id, $userChildCats)){
                        $category->checked = false;
                        $category->subcategory[$k]->checked = false;
                    }
                    else{
                        $category->subcategory[$k]->checked = true;
                    }
                }
            }
            $userCategories[] = $category;
        }
        return $userCategories;
    }

    public function add_user_category(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();
        $users = User::where('id',$user->id)->first();
        $contact = EmergencyContact::where('user_id',$user->id)->first();

        $childCats = [];

        if($request->get('category')) {
            foreach($data['category'] as $category){
                if($category){
                    foreach($category['subcategory'] as $subcategory){
                        if($subcategory){
                            $add = new UserCategory();
                            $add->user_id = $user->id;
                            $add->subcategory_id = $subcategory['subcategory_id'];
                            $add->category_id = $category['category_id'];
                            $add->save();

                            $childCats[] = $subcategory['subcategory_id'];
                        }
                    }
                }
            }
        }
        if($users && isset($users->is_witness) && $contact)
        {
            $user->is_first_login = '0';
            $user->save();
        }
        else{
            $user->is_first_login = '1';
            $user->save();
        }

        $userCategories = $this->getUserSelectedCats($childCats);
        return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"User Category Add Successful",'data' => $userCategories]);
    }

    public function update_user_category(Request $request){
        $user = Auth::user();
        $user_category = UserCategory::where('user_id',$user->id)->get();
        $parentCats = $user_category->pluck('category_id')->toArray();
        $parentCats = array_values(array_unique($parentCats));
        $childCats = $user_category->pluck('subcategory_id')->toArray();

        $data = $request->all();
        if($request->get('category')){
            $availChildCats = [];
            foreach ($data['category'] as $k => $category){
                if(!in_array($category['category_id'], $parentCats)){
                    if(!empty($category['subcategory'])){
                        foreach ($category['subcategory'] as $subcategory){
                            $add = new UserCategory();
                            $add->user_id = $user->id;
                            $add->subcategory_id = $subcategory['subcategory_id'];
                            $add->category_id = $category['category_id'];
                            $add->save();
                            array_push($availChildCats, $subcategory['subcategory_id']);
                        }
                    }
                }
                else{
                    $newSubCats = [];
                    $currSubCats = [];
                    if(!empty($category['subcategory']) && count($category['subcategory']) > 0){
                        foreach ($category['subcategory'] as $subcategory){
                            if(!in_array($subcategory['subcategory_id'], $childCats)){
                                $newSubCats[] = $subcategory['subcategory_id'];
                            }
                            else{
                                $currSubCats[] = $subcategory['subcategory_id'];
                            }
                        }

                        foreach ($newSubCats as $newSubCatId){
                            $add = new UserCategory();
                            $add->user_id = $user->id;
                            $add->subcategory_id = $newSubCatId;
                            $add->category_id = $category['category_id'];
                            $add->save();
                        }
                        $currSubCats = array_merge($currSubCats, $newSubCats);
                        $availChildCats = array_merge($availChildCats, $currSubCats);
                    }
                }
            }

            // Deleting unselect / removed categories
            UserCategory::where('user_id',$user->id)->whereNotIn('subcategory_id', $availChildCats)->delete();

            $userCategories = $this->getUserSelectedCats($availChildCats);
            return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>" User Category Update Successful",'data' => $userCategories]);
        }
        else{
            return response()->json(['status'=> true,'statusCode' => '400' ,"message" =>" Please provide category",'data' => null]);
        }
    }
}
