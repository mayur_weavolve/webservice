<?php

namespace App\Http\Controllers\Api;
use Illuminate\Support\Facades\Auth; 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\SosRequest;
use App\Responder;
use App\User;
use Validator;
use DB;
use App\Request_lead;
class RequestController extends Controller
{
    public function generate_request(Request $request){
        // return $request;
        $reqID=0;$reqCheck=false;$leadCheck=false;
          $validator = Validator::make($request->all(), [
              'latitude'=> 'required',
              'longitude'=> 'required',
              'status'=> 'required',
          ]);
          if ($validator->fails()) {
              return response()->json(['status'=>false,'statusCode' => '400' ,"message" =>"Somthing Wrong Please try again",'data' => $validator->errors()]);
          }else{

                $req = new SosRequest;
                $req->user_id =Auth::user()->id;
                $req->latitude = $request->get('latitude');
                $req->longitude = $request->get('longitude');
                $req->status = $request->get('status');
                if($req->save()){
                    $reqID=$req->id;
                    $reqCheck=true;
                        $responderDetail = DB::table('responders')
                        ->join('users', 'responders.user_id', '=', 'users.id')
                        ->select("*", DB::raw("6371 * acos(cos(radians(" . $request->get('latitude') . "))
                        * cos(radians(latitude)) * cos(radians(longitude) - radians(" . $request->get('longitude') . "))
                        + sin(radians(" . $request->get('latitude'). ")) * sin(radians(latitude))) AS distance"))
                        ->having("distance", "<", 100)
                        ->orderBy("distance")->get();
                        // $users = Responder::with('user')->join()->where('user_id',$request->get('user_id'))->get();

                        foreach($responderDetail as $d){
                            $req = new Request_lead;
                            $req->request_id = $reqID;
                            $req->user_id = $d->id;
                            $req->latitude = $d->latitude;
                            $req->longitude = $d->longitude;
                            $req->status = "accpet";
                            if($req->save()){
                             $leadCheck=true;
                            }
                        }
                }
            if($reqCheck && $leadCheck){
                return response()->json(['status'=>true,'statusCode' => '200' ,"message" =>"request and lead request generated successfully"]);
            }
  
          }
          
        return response()->json(['status'=>true,'statusCode' => '200' ,"message" =>"only request generated successfully"]);

        }


        public function get_responders($id){
         $responderDetail = DB::table('sos_requests')
            ->join('request_leads', 'request_leads.request_id', '=', 'sos_requests.id')
            ->join('users', 'users.id', '=', 'request_leads.user_id')
            ->where('sos_requests.id','=',$id)
            ->get();
        if($responderDetail->isEmpty()){
            return response()->json(["status"=>"400","message"=>"responders not found"]);
        }
        else{
            return response()->json(["status"=>"200","responders"=>$responderDetail]);

        }
        }


}
