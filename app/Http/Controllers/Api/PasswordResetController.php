<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Hash;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Notifications\PasswordResetRequest;
use Illuminate\Support\Facades\Auth;
use App\Notifications\PasswordResetSuccess;
use App\User;
use App\PasswordReset;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PasswordResetController extends Controller
{
    public function create(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
        ]);
        $user = User::where('email', $request->email)->first();
        if (!$user)
        {
            return response()->json(['status'=>false,'statusCode' => '400','message' => 'We cant find a user with that e-mail address.','data' =>  null]);
        }
        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],['email' => $user->email,'token' => Str::random(60)]
        );
        if ($user && $passwordReset)
        {
            $user->notify(
                new PasswordResetRequest($passwordReset->token)
            );
        }
        return response()->json(['status'=>true,'statusCode' => '200','data' =>  $user->email,'message' => 'We have e-mailed your password reset link!']);
    }

    public function reset(Request $request){
        //dd($request->all());
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'token' => 'required|string',
            'password' => 'required|string|confirmed'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator->errors())
                ->withInput();
        }

        $password = $request->get('password');

        // Validate the token
        $tokenData = DB::table('password_resets')->where('token', $request->get('token'))->first();

        // Redirect the user back to the password reset request form if the token is invalid
        if (!$tokenData) return redirect()->back()->withErrors(['email' => 'Invalid token provided']);

        //DB::enableQueryLog(); // Enable query log

        $user = User::where('email', $tokenData->email)->first();
        // Redirect the user back if the email is invalid
        if (!$user) return redirect()->back()->withErrors(['email' => 'Email not found']);

        //Hash and update the new password
        $user->password = Hash::make($password);
        $user->update(); //or $user->save();
        //dd(DB::getQueryLog());

        //Delete the token
        DB::table('password_resets')->where('email', $user->email)
            ->delete();

        //Send Email Reset Success Email
        $user->notify(
            new PasswordResetSuccess($request->get('token'))
        );
        Session::flash('message', 'Password changed successfully');
        return redirect(route('password.success'));
    }

    public function reset_success(){
        return view('auth.passwords.success');
    }
}
