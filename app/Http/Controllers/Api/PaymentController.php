<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use Validator;
use Carbon\Carbon;
use App\Payment;
use App\Advert;

class PaymentController extends Controller
{
    public function payment_add(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();

        $data['user_id'] = $user->id;
        $payment = Payment::create($data);
        $advert = Advert::where('id',$data['advert_id'])->first();
        $date = Carbon::createFromFormat('Y-m-d', $advert->expiry_date);
        $daysToAdd = 1;
        $date = $date->addMonth($daysToAdd);
        $advert->expiry_date = $date;
        $advert->save();
        return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Payment Add Successfully....!",'data' =>$payment]);
    }
}
