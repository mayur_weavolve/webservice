<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ReachOption;

class ReachOptionController extends Controller
{
    public function list(Request $request)
    {
        $data = $request->all();

        $reach = $data['reach'];
        if($reach <> 'usa'){
            $reach = 'kenya';
        }

        $option = ReachOption::where('reach',$reach)->get();
        if(count($option))
        {
            return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>" Reach Option Liting....",'data' => $option]);
        }
        else{
            return response()->json(['status'=> false,'statusCode' => '400' ,"message" =>"No Data Found....",'data' => null]);
        }
    }
}
