<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Support;
use App\Responder;
use App\SosRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ResponderController extends Controller
{
    public function register_responder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            //'user_id' => 'required',
            'responder_type' => 'required',
            'uniform_photo' => 'required',
            'employer_name' => 'required',
            // 'vehicle_number' => 'required',
            'vehicle_photo' => 'required',
            //'status' => 'required',
            'rejection_reason' => 'nullable',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'mobileno' => 'required',

        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'statusCode' => '400', "message" => "Somthing Wrong Please try again", 'data' => $validator->errors()]);
        } else {
            // $find_user = Responder::with('user')->where('user_id', $request->get('user_id'))->get();

            // if (count($find_user) > 0)
            // {
            //     return response()->json(['status' => true, 'statusCode' => '400', "message" => "user not found"]);
            // }
            // else
            // {
            $check = Responder::where('user_id', Auth::user()->id)->first();
            if ($check)
            {
                return response()->json(['status' => true, 'statusCode' => '400', "message" => "Responder already register"]);
            }
            else
            {
                $responder = new Responder;
                $responder->user_id = Auth::user()->id;

                $responder->responder_type = $request->get('responder_type');
                $responder->employer_name = $request->get('employer_name');
                $responder->vehicle_number = $request->get('vehicle_number');
                $responder->status = "pending";
                $responder->rejection_reason = $request->get('rejection_reason');

                if ($request->hasfile('uniform_photo')) {
                    $file = $request->file('uniform_photo');
                    $extension = $file->getClientOriginalExtension();
                    $filename = 'uniform-' . time() . '.' . $extension;
                    $file->move('images/uniform/', $filename);
                    $responder->uniform_photo = $filename;
                }

                if ($request->hasfile('vehicle_photo')) {
                    $file = $request->file('vehicle_photo');
                    $extension = $file->getClientOriginalExtension();
                    $filename =  'vehicle-' . time() . '.' . $extension;
                    $file->move('images/vehicle/', $filename);
                    $responder->vehicle_photo = $filename;
                }

                $responder->save();

                $user = Auth::user();
                $user->first_name = $request->get('first_name');
                $user->last_name = $request->get('last_name');
                $user->email = $request->get('email');
                $user->mobileno = $request->get('mobileno');
                $user->save();

                return response()->json(['status' => true, 'statusCode' => '200', "message" => "responder register successfully"]);
            }
        }
    }


    public function get_requests($id)
    {
        $requestDetail = DB::table('responders')
            ->join('request_leads', 'request_leads.user_id', '=', 'responders.user_id')
            ->where('responders.user_id', '=', $id)
            ->select('responders.user_id', 'request_leads.request_id', 'request_leads.latitude', 'request_leads.longitude', 'request_leads.status')
            ->get();

        if ($requestDetail->isEmpty()) {
            return response()->json(["status" => "400", "message" => "you dont have any request"]);
        } else {
            return response()->json(["status" => "200", "requests" => $requestDetail]);
        }
    }

    public function update_status(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'responder_id' => 'required|numeric',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'statusCode' => '400', "message" => "Somthing Wrong Please try again", 'data' => $validator->errors()]);
        } else {
            $findId = Responder::where('id', $request->get('responder_id'))->first();
            if (!empty($findId)) {
                $findId->status = $request->get('status');
                if ($findId->save()) {
                    return response()->json(["status" => "200", "messsage" => "status updated successfully"]);
                } else {
                    return response()->json(["status" => "400", "messsage" => "status not updated"]);
                }
            } else {
                return response()->json(["status" => "400", "messsage" => "responder not found"]);
            }
        }
    }
}
