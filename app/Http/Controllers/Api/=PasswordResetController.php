<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Notifications\PasswordResetRequest;
use Illuminate\Support\Facades\Auth; 
use App\Notifications\PasswordResetSuccess;
use App\User;
use App\PasswordReset;

class PasswordResetController extends Controller
{
    public function create(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
        ]);
        $user = User::where('email', $request->email)->first();
        if (!$user)
        {
            return response()->json(['status'=>false,'statusCode' => '400','message' => 'We cant find a user with that e-mail address.','data' =>  null]);
        }
        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],['email' => $user->email,'token' => str_random(60)]
        );
        if ($user && $passwordReset)
        {
            $user->notify(
                new PasswordResetRequest($passwordReset->token)
            );
        }
        return response()->json(['status'=>true,'statusCode' => '200','data' =>  $user->email,'message' => 'We have e-mailed your password reset link!']);
    }
}
