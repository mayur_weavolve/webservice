<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\Http\Controllers\Controller;
use App\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Validator;
use Config;
use Carbon\Carbon;
use App\Advert;
use App\User;
use App\UserCategory;
use App\AdvertsBlockRequest;

class AdvertController extends Controller
{
    public function list(Request $request)
    {
        $user = Auth::user();
        //$user = User::find(10);
        $data = $request->all();
        $Categories = UserCategory::select(\DB::raw("GROUP_CONCAT(user_categories.category_id) as category"))
        ->where('user_categories.user_id',$user->id)
        ->groupBy("user_categories.user_id")
        ->get()
        ->first();

        $SubCategories = UserCategory::select(\DB::raw("GROUP_CONCAT(user_categories.subcategory_id) as subcategory"))
        ->where('user_categories.user_id',$user->id)
        ->groupBy("user_categories.user_id")
        ->get()
        ->first();

        $catCondi = "";
        $subCatCondi = "";
        if(isset($Categories['category']) && $Categories['category']){
            $catCondi = ' AND category_id IN (' . $Categories['category'] . ')';

        }

        if(isset($SubCategories['subcategory']) && $SubCategories['subcategory']){
            $subCatCondi = ' AND subcategory_id IN (' . $SubCategories['subcategory'] . ')';
        }

        $country =  "";
        if(isset($data['country_id']) && $data['country_id']){
            $subCatCondi = ' OR country_id = ' . $data['country_id'];
        }
        if(isset($data['state_id']) && $data['state_id']){
            $subCatCondi = ' OR state_id = ' . $data['state_id'];
        }

        //$ads = \DB::select(\DB::raw('SELECT  adverts.id, latitude, longitude, `option`, category_id, subcategory_id, (6371 * ACOS( COS( RADIANS('.$data['latitude'].') ) * COS( RADIANS( latitude ) ) * COS( RADIANS( longitude ) - RADIANS('.$data['longitude'].') ) + SIN( RADIANS('.$data['latitude'].') ) * SIN( RADIANS( latitude ) ) ) ) AS distance FROM `adverts` JOIN reach_options ON reach_options.id = `adverts`.`reach_id` WHERE (reach_options.id IN (1, 2, 3, 6, 7, 8) '.$catCondi.$subCatCondi.') '.$country.' HAVING distance <= `option`'));

        $query = 'SELECT  adverts.id, adverts.latitude, adverts.longitude, reach_options.option, adverts.category_id, adverts.subcategory_id, (6371 * ACOS( COS( RADIANS('.$data['latitude'].') ) * COS( RADIANS( latitude ) ) * COS( RADIANS( longitude ) - RADIANS('.$data['longitude'].') ) + SIN( RADIANS('.$data['latitude'].') ) * SIN( RADIANS( latitude ) ) ) ) AS distance FROM adverts JOIN reach_options ON reach_options.id = adverts.reach_id WHERE (reach_options.id IN (1, 2, 3, 6, 7, 8) '.$catCondi.$subCatCondi.') '.$country.' HAVING distance <= reach_options.option';

        $ads = \DB::select(\DB::raw($query));

        $ids = [];
        foreach($ads as $ad){
            $ids[] = $ad->id;
        }
        $advert = Advert::orderBy("id", "desc")
                        ->where('status',1)
                        ->where('is_blocked', 0)
                        ->whereIn('id', $ids)
                        ->paginate(20);

            foreach($advert as $adverts)
            {
                if($adverts->photo)
                {
                    $adverts['photo'] = Config::get('constants.image').$adverts->photo;
                }
                if($adverts->flayer_photo)
                {
                    $adverts['flayer_photo'] = Config::get('constants.image').$adverts->flayer_photo;
                }
            }
            return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Advert Liting....",'data' => $advert]);
    }
    public function add(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();
        $list = Advert::where('user_id',$user->id)
                        ->where('status','1')
                        ->whereIn('reach_id',[1, 6])
                        ->count();
        if($list >= 3)
        {
           return response()->json(['status'=> false,'statusCode' => '400' ,"message" =>"You have reached limit for free adverts. You can add new free adverts when any existing advert expires or delete existing advert and then add new advert.",'data' =>null]);
        }


        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'description' => 'required',
            'price' => 'required',
            'name' => 'required',
            'address' => 'required',
            'mobileno'=>'required',
            'reach_id'=>'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=> false,'ErrorCode' => '400' ,"message" =>"Somthing Wrong Please try again",'data' => $validator->errors()]);
        }
        if($request->file('photo'))
        {
            $data['photo'] = $request->file('photo')->hashName();
            $request->file('photo')
            ->store('images', ['disk' => 'public']);
        }
        if($request->file('flayer_photo'))
        {
            $data['flayer_photo'] = $request->file('flayer_photo')->hashName('');
            $request->file('flayer_photo')
            ->store('images', ['disk' => 'public']);
        }
       /* if($data['photo'] != null)
        {
            $folderPath = public_path() ."/images/";

            $image_parts = explode(";base64,", $data['photo']);
            $image_type_aux = explode("images/", $image_parts[0]);
            $image_type = isset($image_type_aux[1]) ? $image_type_aux[1] : "png";
            $image_base64 = base64_decode($image_parts[1]);
            $fileName = uniqid() . '.'.$image_type;
            $file = $folderPath . $fileName;

            file_put_contents($file, $image_base64);
            $data['photo'] = $fileName;
        }
        if($data['flayer_photo'] != null)
        {
            $folderPath = public_path() ."/images/";

            $image_parts = explode(";base64,", $data['flayer_photo']);
            $image_type_aux = explode("images/", $image_parts[0]);
            $image_type = isset($image_type_aux[1]) ? $image_type_aux[1] : "png";
            $image_base64 = base64_decode($image_parts[1]);

            $fileName = uniqid() . '.'.$image_type;
            $file = $folderPath . $fileName;

            file_put_contents($file, $image_base64);
            $data['flayer_photo'] = $fileName;
        } */
        $data['user_id'] = $user->id;
        $data['status'] = 1;
        $advert = Advert::create($data);
        $advert->expiry_date = $advert->created_at->addMonth(1);
        $advert->save();

        // Sending notifications within 2 km of users with same category and sub category
        $users = \DB::select(\DB::raw('SELECT  users.*, (6371 * ACOS(
            COS( RADIANS(' . $data['latitude'] . ') )
        * COS( RADIANS( latitude ) )
        * COS( RADIANS( longitude ) - RADIANS(' . $data['longitude'] . ') )
        + SIN( RADIANS(' . $data['latitude'] . ') )
        * SIN( RADIANS( latitude ) )
            ) ) AS distance FROM `users`
            JOIN user_categories
            ON user_categories.user_id = users.id
            WHERE user_categories.category_id = '.$data['category_id'].'
            AND user_categories.subcategory_id = '.$data['subcategory_id'].'
        HAVING distance <= 2'));

        $ids = [];
        foreach($users as $user){
            if($user->device_token){
                $ids[]  = $user->device_token;
            }
        }

        $category = Category::where('id',$data['category_id'])->value('name');
        $subcategory = SubCategory::where('id',$data['subcategory_id'])->value('name');

        $notify_title = 'New Advert';
        $notify_msg = 'New Advert is added for '.$subcategory.' - '.$category;

        // Android Notification
        $notification = array(
            "title" => $notify_title,
            "subject" => $notify_msg,
            "notification_type" => 3,
            "body" => $notify_msg,
            "mutable-content" => 1
        );
        $this->sendFCMMessage( FCM_SERVER_KEY, $notification , $ids);

        return response()->json(['status'=> true,'statusCode' => '200' ,"message" => "Advert add successful",'data' => $advert]);

    }
    public function update(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();

        $advert = Advert::where('id',$data['advert_id'])->first();
        if($advert)
        {
            $advert->user_id = $user->id;
            $advert->category_id = $request->input('category_id');
            $advert->subcategory_id = $request->input('subcategory_id');
            $advert->description = $request->input('description');
            $advert->price = $request->input('price');
            $advert->name = $request->input('name');
            $advert->address = $request->input('address');
            $advert->mobileno = $request->input('mobileno');
            $advert->latitude = $request->input('latitude');
            $advert->longitude = $request->input('longitude');
            $advert->reach_id = $request->input('reach_id');
            $advert->photo = $request->input('photo');
            $advert->flayer_photo = $request->input('flayer_photo');
            $advert->country_id = $request->input('country_id');
            $advert->state_id = $request->input('state_id');
            // if($data['photo'] != null)
            // {
            //     $advert->photo = $request->file('photo')->hashName();
            //     $request->file('photo')
            //     ->store('image', ['disk' => 'public']);
            // }
            // if($data['flayer_photo'] != null)
            // {
            //     $advert->flayer_photo = $request->file('flayer_photo')->hashName('');
            //     $request->file('flayer_photo')
            //     ->store('image', ['disk' => 'public']);
            // }
            // if($request->input('photo') != null)
            // {
            //     $folderPath = public_path() ."/images/";

            //     $image_parts = explode(";base64,", $data['photo']);
            //     $image_type_aux = explode("image/", $image_parts[0]);
            //     $image_type = $image_type_aux[1];
            //     $image_base64 = base64_decode($image_parts[1]);
            //     $file = $folderPath . uniqid() . '. '.$image_type;

            //     file_put_contents($file, $image_base64);
            // }
            // if($request->input('flayer_photo') != null)
            // {
            //     $folderPath = public_path() ."/images/";

            //     $image_parts = explode(";base64,", $data['flayer_photo']);
            //     $image_type_aux = explode("image/", $image_parts[0]);
            //     $image_type = $image_type_aux[1];
            //     $image_base64 = base64_decode($image_parts[1]);
            //     $file = $folderPath . uniqid() . '. '.$image_type;

            //     file_put_contents($file, $image_base64);
            // }
            $advert->save();
        }

        return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Advert update successful",'data' =>$advert]);

    }
    public function search(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();
        $search = $request->get('search');
        $advert = Advert::select('adverts.*')
                        ->with('category','subcategory')
                        ->join('categories','categories.id','adverts.category_id')
                        ->join('sub_categories','sub_categories.id','adverts.subcategory_id')
                        ->where('adverts.status',1)
                        ->where('adverts.is_blocked',0)
                        ->where(function($query) use ($search){
                            $query->where('adverts.description', 'like', "{$search}%");
                            $query->orWhere('adverts.name', 'like', "{$search}%");
                            $query->orWhere('categories.name', 'like', "{$search}%");
                            $query->orWhere('sub_categories.name', 'like', "{$search}%");
                        })
                        ->paginate(20);

        if(count($advert) > 0)
        {
            foreach($advert as $adverts)
            {
                if($adverts->photo)
                {
                    $adverts->photo = Config::get('constants.image').$adverts->photo;
                }
                if($adverts->flayer_photo)
                {
                    $adverts->flayer_photo = Config::get('constants.image').$adverts->flayer_photo;
                }
            }
            return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Advert Liting....",'data' => $advert]);
        }
        else{
            return response()->json(['status'=> false,'statusCode' => '400' ,"message" =>"No Data Found....",'data' => null]);
        }
    }
    public function myadvert(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();
        $advert = Advert::where('user_id',$user->id)->get();
        if(count($advert))
        {
            foreach($advert as $adverts)
            {
                if($adverts->photo)
                {
                    $adverts['photo'] = Config::get('constants.image').$adverts->photo;
                }
                if($adverts->flayer_photo)
                {
                    $adverts['flayer_photo'] = Config::get('constants.image').$adverts->flayer_photo;
                }
            }
            return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Advert Liting....",'data' => $advert]);
        }
        else{
            return response()->json(['status'=> false,'statusCode' => '400' ,"message" =>"No Data Found....",'data' => null]);
        }
    }
    public function advert_update_status(Request $request)
    {
        $data = $request->all();
        $advert = Advert::where('id',$data['advert_id'])->first();
        $advert->status = $request->input('status');
        $advert->save();
        return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Advert update successful",'data' =>$advert]);

    }
    public function delete(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();
        $advert = Advert::where('id',$data['advert_id'])->first();
        if($advert)
        {
            $advert->delete();
            return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Advert delete successful",'data' =>null]);
        }
        else{
            return response()->json(['status'=> false,'statusCode' => '400' ,"message" =>"No data Found....!",'data' =>null]);
        }
    }
    public function createImage($img)
    {

        $folderPath = public_path() ."images/";

        $image_parts = explode(";base64,", $img);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $file = $folderPath . uniqid() . '. '.$image_type;

        file_put_contents($file, $image_base64);

        return $file;

    }

    public function advert_block_request(Request $request){
        $data = $request->all();
        $user = Auth::user();
        //echo json_encode($user);exit;

        $validator = Validator::make($request->all(), [
            'advert_id' => 'required',
            'reason' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=> false,'ErrorCode' => '400' ,"message" =>"Somthing Wrong Please try again",'data' => $validator->errors()]);
        }

        $data['user_id'] = $user->id;
        $adv_request = AdvertsBlockRequest::where("advert_id",$data['advert_id'])->count();
        if($adv_request <= 0){
            AdvertsBlockRequest::create($data);
            return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Advert reported successfully as illegal",'data' =>null]);
        }
        else{
            return response()->json(['status'=> false,'statusCode' => '400' ,"message" =>"This advert has been already reported as illegal by other user",'data' =>null]);
        }
    }

    public function sendFCMMessage($server_key,$notification, $target){
        $url = 'https://fcm.googleapis.com/fcm/send';
        if(is_array($target)){
            $arrayToSend = array('registration_ids' => $target,
                'data' => $notification,
                'notification' => $notification,
                'priority'=>'high');
        }else{
            $arrayToSend = array('to' => $target, 'notification' => $notification,'priority'=>'high');
        }
        //header with content_type api key
        $json = json_encode($arrayToSend);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key='. $server_key;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
        //Send the request
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Oops! FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
        exit;
    }
}
