<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\UserCategory;
use App\EmergencyContact;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;
use Config;
use DB;
use App\Setting;

class UserController extends Controller
{
    public function register(Request $request)
    {
        /*$validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'mobileno' => 'required',
            'is_social_login'=>'required',
        ],
        [
            'email.unique' => 'The email has already been registered.'
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>false,'statusCode' => '400' ,"message" =>"Somthing Wrong Please try again",'data' => $validator->errors()]);
        }*/

        $isDuplicateRecord = User::where('email',$request->get('email'))->count();
        if($isDuplicateRecord > 0){
            return response()->json(['status'=> false,'statusCode'=> '400',"message" => "The email has already been registered.","data"=> null]);
        }

        $data = $request->all();
        $data['password'] = bcrypt($data['password']);
        $data['is_first_login'] = '1';
        $data['is_password_change'] ='0';
        $user = User::create($data);

        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['userid'] =  $user->id;
        $success['first_name'] =  $user->first_name;
        $success['last_name'] =  $user->last_name;
        $success['email'] =  $user->email;
        $success['mobileno'] =  $user->mobileno;
        $success['is_social_login'] =  $user->is_social_login;
        $success['is_password_change'] =  $user->is_password_change;
        $success['is_first_login'] =  $user->is_first_login;

        return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"User registered successfully....!",'data' =>$success]);
    }
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
            'device_id'=> 'required',
            'device_token'=> 'required',
            'is_social_login'=> 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>false,'statusCode' => '400' ,"message" =>"Somthing Wrong Please try again",'data' => $validator->errors()]);
        }
        if(Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')]))
        {
            $user = Auth::user();
            $users = User::where('id',$user->id)->get();
            $user->device_id = $request->input('device_id');
            $user->device_token = $request->input('device_token');
            $user->is_social_login = $request->input('is_social_login');
            $user->save();
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            // $success['data'] = $users;
            $success['user_id'] =  $user->id;
            $success['first_name'] =  $user->first_name;
            $success['last_name'] =  $user->last_name;
            $success['email'] =  $user->email;
            $success['mobileno'] =  $user->mobileno;
            if($user->profile_photo)
            {
                $success['profile_photo'] = Config::get('constants.image').$user->profile_photo;
            }
            // $success['profile_photo'] =  $user->profile_photo;
            $success['is_first_login'] =  $user->is_first_login;
            $success['is_social_login'] =  $user->is_social_login;
            $success['is_password_change'] =  $user->is_password_change;

            if($user->is_first_login == 1){
                $success['welcome_message'] = Setting::get('app_welcome_msg');
            }

            $success['marketplace_message'] = Setting::get('app_marketplace_msg');
            $message =  "Login successfuly";
            // $user->is_first_login =  '0';
            // $user->save();
            return response()->json(['status'=> true,'statusCode' => '200','message ' => $message,'data' =>  $success]);
        }
        else{
            return response()->json(['status'=> false,'statusCode'=> '400',"message" => "Invalid username or password.","data"=> null]);
        }
    }
    public function change_password(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();
        $validatedData = $request->validate([
            'current_password' => 'required',
            'new_password' => 'required',
        ]);

        if (!(Hash::check($request->get('current_password'), $user->password))) {
            return response()->json(['status'=> false,'statusCode'=> '400',"message" =>"Your current password does not matches with the password you provided. Please try again.", "data"=> null]);
        }

        if(strcmp($request->get('current_password'), $request->get('new_password')) == 0){
            return response()->json(['status'=> false,'statusCode'=> '400',"message" => "New Password cannot be same as your current password. Please choose a different password.", "data"=> null]);
        }

        $user->password = bcrypt($request->get('new_password'));
        $user->save();
        $success['userId'] = $user->id;
        $success['email'] = $user->email;
        return response()->json(['status'=> true,'statusCode' => '200','message' => "password has been Change successfully.",'data' =>$success]);
    }
    public function edit_profile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'email' => 'required',
            'mobileno' => 'required',
            'last_name' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>false,'statusCode' => '400' ,"message" =>"Somthing Wrong Please try again",'data' => $validator->errors()]);
        }
            $data = $request->all();
            $user = Auth::user();
            $user->first_name=$request->input('first_name');
            $user->last_name=$request->input('last_name');
            $user->email=$request->input('email');
            $user->mobileno=$request->input('mobileno');
            $user->save();
            if($user->profile_photo)
            {
                $user->profile_photo = Config::get('constants.image').$user->profile_photo;
            }
            return response()->json(['status'=>true,'statusCode' => '200','data' =>  $user,'message' => "Profile has been changed successfully."]);
    }
    public function add_location(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longitude' => 'required',

        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>false,'statusCode' => '400' ,"message" =>"Somthing Wrong Please try again",'data' => $validator->errors()]);
        }
        $user = Auth::user();
        $data = $request->all();
        $user->latitude = $request->input('latitude');
        $user->longitude = $request->input('longitude');
        $user->save();

        return response()->json(['status'=>true,'statusCode' => '200','message' => "User Location Add successfully.",'data' =>  $user]);
    }

    public function witness(Request $request)
    {
        $user = Auth::user();
        $users = User::where('id',$user->id)->first();
        $user_category = UserCategory::where('user_id',$user->id)->first();
        $contact = EmergencyContact::where('user_id',$user->id)->first();
        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'is_witness' => 'required',

        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>false,'statusCode' => '400' ,"message" =>"Somthing Wrong Please try again",'data' => $validator->errors()]);
        }
        $user->is_witness = $request->input('is_witness');
        $user->save();
        if($users && $contact && $user_category)
        {
            $user->is_first_login = '0';
            $user->save();
        }
        else{
            $user->is_first_login = '1';
            $user->save();
        }
        return response()->json(['status'=>true,'statusCode' => '200','message' => "User is Witness.",'data' =>  $user]);

    }
    public function sos(Request $request)
    {
        $loginUser = Auth::user();
       // $users = User::get();

        $data = $request->all();


        $users = \DB::select(\DB::raw('SELECT  users.*, (6371 * ACOS(
            COS( RADIANS(' . $data['latitude'] . ') )
        * COS( RADIANS( latitude ) )
        * COS( RADIANS( longitude ) - RADIANS(' . $data['longitude'] . ') )
        + SIN( RADIANS(' . $data['latitude'] . ') )
        * SIN( RADIANS( latitude ) )
            ) ) AS distance FROM `users`
            WHERE is_witness = 1
        HAVING distance <= 0.1'));

        $ids = [];
        $userEmailIds = [];
        foreach($users as $user){
            if($user->id != $loginUser->id && $user->device_token){
                $ids[]  = $user->device_token;
                $userEmailIds[] = $user->email;
            }
        }

        $notify_msg = $loginUser->first_name." ".$loginUser->last_name." pressed SOS near you. Record video ?";
        $notify_title = $loginUser->first_name." ".$loginUser->last_name;

        // Android Notification
        $notification = array(
            "message" => $notify_msg,
            "name" =>  $notify_title,
            "latitude" => $request->get('latitude'),
            "longitude" => $request->get('longitude'),
            "id" => $loginUser->id,
            "notification_type" => 1,
            "body" => $notify_msg,
            "image" => '',
            "mutable-content" => 1
        );
        $this->sendFCMMessage( FCM_SERVER_KEY, $notification , $ids);

        // $latitude = $user->latitude;
        // $longitude = $user->longitude;
        // $distance = 100;
        // $results = DB::select(DB::raw('SELECT id,device_token, ( 3959 * acos( cos( radians(' . $latitude . ') )
        //                 * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $longitude . ') )
        //                 + sin( radians(' . $latitude .') ) * sin( radians(latitude) ) ) ) AS distance FROM users
        //                 HAVING distance <= ' . $distance . ' AND distance > 0 ORDER BY distance' ));

        return response()->json(['status'=>true,'statusCode' => '200','message' => "User Find",'data' => $ids, 'email_ids' => $userEmailIds]);

    }

    public function sosNew(Request $request)
    {
        $loginUser = Auth::user();

        $data = $request->all();

        $latitude = $data['latitude'];
        $longitude = $data['longitude'];


         $req = new SosRequest();
         $req->user_id = $loginUser->id;
         $req->latitude = $data['latitude'];
         $req->longitude = $data['longitude'];
         $req->status = "pending";
         $req->save();


        $users = DB::select(DB::raw('SELECT users.id,
                        responders.id AS responders_id,
                        responder_type,
                        device_token, ( 3959 * acos( cos( radians(' . $latitude . ') )
                        * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $longitude . ') )
                        + sin( radians(' . $latitude .') ) * sin( radians(latitude) ) ) ) AS distance FROM users
                        JOIN responders ON responders.user_id = users.id AND
                        responders.status = "approved" AND users.id != ' . Auth::user()->id . '
                         HAVING
                        (distance <= 200 and  responder_type = "patrol man") OR
                        (distance <= 1000 and  responder_type = "patrol car")

                        ORDER BY distance' ) );
        
        $ids = [];
        foreach ($users as $user) {

            $req_lead = new Request_lead();
            $req_lead->user_id = $user->id;
            $req_lead->request_id = $req->id;
            $req_lead->latitude = $latitude;
            $req_lead->longitude = $longitude;
            $req_lead->status = "pending";
            $req_lead->save();

            if($user->device_token){
                $ids[]  = $user->device_token;
            }
        }
        $notification = array(
            "message" => $loginUser->first_name . " " . $loginUser->last_name . " pressed SOS near you. Record video ?",
            "name" =>  $loginUser->first_name . " " . $loginUser->last_name,
            "latitude" => $request->get('latitude'),
            "longitude" => $request->get('longitude'),
            "id" => $loginUser->id,
            "lead_id" => $req->id,
            "notification_type" => 2
        );
        if($ids){
            $this->sendFCMMessage(FCM_SERVER_KEY, $notification, $ids);
        }

        /* Old SOS Fearure */
//id NOT IN (SELECT user_id from responders WHERE status != "approved")
        $users = \DB::select(\DB::raw('SELECT  users. * , (6371 * ACOS(COS( RADIANS(' . $data['latitude'] . ') )
        * COS( RADIANS( latitude ) ) * COS( RADIANS( longitude ) - RADIANS(' . $data['longitude'] . ') )
        + SIN( RADIANS(' . $data['latitude'] . ') ) * SIN( RADIANS( latitude ))))
        AS distance FROM `users` WHERE is_witness = 1
        
        HAVING distance > 0.1'));
        $ids = [];
        foreach ($users as $user) {
            if($user->device_token){
                $ids[]  = $user->device_token;
            }
        }

        $notification = array(
            "message" => $loginUser->first_name . " " . $loginUser->last_name . " pressed SOS near you. Record video ?",
            "name" =>  $loginUser->first_name . " " . $loginUser->last_name,
            "latitude" => $request->get('latitude'),
            "longitude" => $request->get('longitude'),
            "id" => $loginUser->id,
            "notification_type" => 1
        );
        $this->sendFCMMessage(FCM_SERVER_KEY, $notification, $ids);


        return response()->json(['status' => true, 'statusCode' => '200', 'message' => "User Find", 'data' => []]);
    }







    public function profile_pic_edit(Request $request)
    {
        $user = Auth::user();
        if($request->file('profile_photo'))
        {
            $user->profile_photo = $request->file('profile_photo')->hashName();
            $request->file('profile_photo')
            ->store('images', ['disk' => 'public']);
        }
        // $user->profile_photo = $request->input('profile_photo');
        $user->save();
        $user->profile_photo = Config::get('constants.image').$user->profile_photo;
        return response()->json(['status'=>true,'statusCode' => '200','message' => "Profile picture updated successfully.",'data' =>  $user]);

    }


    public function sendFCMMessage($server_key,$notification, $target){
        $url = 'https://fcm.googleapis.com/fcm/send';
        if(is_array($target)){
            $arrayToSend = array('registration_ids' => $target,
            'data' => $notification,
            'notification' => $notification,
            'priority'=>'high');
        }else{
            $arrayToSend = array('to' => $target, 'notification' => $notification,'priority'=>'high');
        }
        //header with content_type api key
         $json = json_encode($arrayToSend);
         $headers = array();
         $headers[] = 'Content-Type: application/json';
         $headers[] = 'Authorization: key='. $server_key;
         $ch = curl_init();
         curl_setopt($ch, CURLOPT_URL, $url);
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
         curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
         curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
         //Send the request
         $result = curl_exec($ch);
        if ($result === FALSE) {
          die('Oops! FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
        exit;
     }

}
