<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Config;

class ImageController extends Controller
{
    public function image_upload(Request $request)
    {
        $data = $request->all();
        $data['image'] = $request->file('image');
        if ($request->file('image')==null){        
        }
        else{
            $data['image'] = $request->file('image')->hashName();
            $request->file('image')
            ->store('images', ['disk' => 'public']);
            $data['url'] = Config::get('constants.image').$data['image'];        
        }
        return response()->json(['status'=>true,'statusCode' => '200',"message" =>"Image upload Successfully....!",'data' =>  $data]);
    }
}
