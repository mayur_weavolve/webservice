<?php

namespace App\Http\Controllers\Api;
use Illuminate\Support\Facades\Auth; 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Request_lead;
use App\SosRequest;
use App\Responder;
use App\Support;
use Validator;
use DB;

class RequestLeadController extends Controller
{
   public function accept_request(Request $request){
    $validator = Validator::make($request->all(), [
        'r_id' => 'required|numeric',
    ]);
    if ($validator->fails()) {
        return response()->json(['status'=>false,'statusCode' => '400' ,"message" =>"Somthing Wrong Please try again",'data' => $validator->errors()]);
    }else{
            $request_lead = Request_lead::where('request_id',$request->get('r_id'))
            ->where('user_id',Auth::user()->id)
            ->first();
            
            if(empty($request_lead)){
                return response()->json(["status"=>"400","message"=> "Request not found"]);
            }
            else
            {
                $sosrequest = SosRequest::find($request->get('r_id'));
                if($sosrequest && $sosrequest->status == "pending"){
                    $sosrequest->status = "accepted";
                    $sosrequest->save();
                   
                    $request_lead->status = "accepted";
                    $request_lead->save();

                    $ticket = new Support;
                    $ticket->request_id = $sosrequest->id;
                    $ticket->responder_id = Auth::user()->id;
                    $ticket->subject ="Auto generated";
                    $ticket->discription ="Add description";
                    $ticket->status= "started";
                    $ticket->save();
                    return response()->json(['status' => true, 'statusCode' => '200', 'message' => "Request Accepted.", 'data' => []]);
                }else{
                    return response()->json(['status' => false, 'statusCode' => '400', 'message' => "Lead Expired", 'data' => []]);
                }
            }
        }

   }

   public function reject_request(Request $request){
    $validator = Validator::make($request->all(), [
        'r_id' => 'required|numeric',
    ]);
    if ($validator->fails()) {
        return response()->json(['status'=>false,'statusCode' => '400' ,"message" =>"Somthing Wrong Please try again",'data' => $validator->errors()]);
    }else{
        $request_lead = Request_lead::where('request_id', $request->get('r_id'))
        ->where('user_id', Auth::user()->id)
        ->first();
         if (empty($request_lead)) {
        return response()->json(["status" => "400", "message" => "request not found"]);
        } 
        else {
        $edit = Request_lead::find($request_lead->id);
        $edit->status = "rejected";
        $edit->save();
        
        return response()->json(['status' => true, 'statusCode' => '200', 'message' => "Request Rejected.", 'data' => []]);

        }

     }
   }



   public function update_status(Request $request){
    $validator = Validator::make($request->all(), [
        'request_id' => 'required|numeric',
        'status'=> 'required',
    ]);
    if ($validator->fails()) {
        return response()->json(['status'=>false,'statusCode' => '400' ,"message" =>"Somthing Wrong Please try again",'data' => $validator->errors()]);
    }
    else{
         $request_lead = Request_lead::where('request_id',$request->get('request_id'))
        ->where('user_id',Auth::user()->id)
        ->first();
      if(!empty($request_lead)){
        $request_lead->status=$request->get('status');
        if($request_lead->save()){
            return response()->json(["status"=>"200","messsage"=>"status updated successfully"]);
        }
        else{
          return response()->json(["status"=>"400","messsage"=>"status not updated"]);
        }
      }
      else{
        return response()->json(["status"=>"400","messsage"=>"request not found"]);

      }
     
    }
  }
}
