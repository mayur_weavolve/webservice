<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Support;
use App\SosRequest;
use App\Request_lead;
use DB;
class SupportController extends Controller
{
    public function ticket_list(){
        $tickets = Support::with('requestInfo','responderInfo')->paginate(3);
        return view('tickets.ticketList', compact('tickets'));
    }

    public function ticket_dispute(){
        $tickets = Support::where('status','dispute')->with('requestInfo','responderInfo')->paginate(3);
        return view('tickets.ticketDispute', compact('tickets'));
    }
    public function ticket_close($id){
        $ticket = Support::find($id);
      $ticket->status="closed";
      $ticket->save();
      return redirect()->back();
    }
   
    public function user_not_respond(){
     $tickets = Support::where('supports.status','!=','closed')->with('leadRequestInfo','requestInfo')
                             ->join('request_leads','supports.responder_id','request_leads.user_id')
                             ->where('request_leads.status','completed')
                              ->whereRaw("DATEDIFF(supports.updated_at,supports.created_at)  > 1")
                             ->select('supports.id','supports.responder_id','supports.request_id','supports.created_at','supports.subject','supports.discription','supports.status')
                             ->distinct('supports.id')
                            ->paginate(3);


         $tickets1 = Support::where('supports.status','!=','closed')->with('requestInfo')->paginate(10);

       return view('tickets.userNotRespond', compact('tickets'));
   // return $tickets[0]->leadRequestInfo->responder;

     }
    
      
    // whereRaw('DATEDIFF(`updated_at` , `created_at` ) >= 1')

     
 
}
