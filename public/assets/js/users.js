$(function() {
    var frmObj = $('#frmBroadCastMsg');
    $("#broadCastBtn").on("click", function () {
        frmObj[0].reset();
        frmObj.find(".msg_row").html('');
        frmObj.find('button').removeAttr('disabled');
    });

    $("#frmBroadCastMsg").submit(function (e) {
        e.preventDefault();
        var frmObj = $('#frmBroadCastMsg');
        frmObj.find('button').attr('disabled','disabled');
        frmObj.find('button[type="submit"]').html('Sending...');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let formData = new FormData(this);
        let flyer_image = $('#flyer_image')[0].files[0];
        formData.append('flyer_image', flyer_image);
        var userIds = $('#frmUsers').serializeArray();
        let users = [];
        $.each(userIds, function(key, userId){
            users.push(userId.value);
        });
        if(users.length){
            formData.append('user_ids', JSON.stringify(users));
        }

        $.ajax({
            url: notifyUrl,
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            success: function (response) {
                frmObj.find('button').removeAttr('disabled').html('Submit');
                if(response.status == false){

                    frmObj.find('.msg_row').html('');
                    if(response.errors){
                        console.log(response.errors);
                        $.each(response.errors, function(k, error){
                            let errMsg = '<div class="alert alert-danger">'+error+'</div>';
                            frmObj.find('.msg_row').append(errMsg);
                        });
                    }
                }
                else{
                    frmObj.find('.msg_row').html('<div class="alert alert-success">'+response.message+'</div>');
                    frmObj[0].reset();
                }

                setTimeout(function(){
                    frmObj.find('.msg_row').html('');
                },'8000');
            }
        });
    });
});
