<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Hash;
use Validator;
use Config;
use App\Advert;
use App\User;
use App\UserCategory;

class AdvertController extends Controller
{
    public function list(Request $request)
    {
        $user = Auth::user();
        //$user = User::find(10);
        $data = $request->all();
        $Categories = UserCategory::select(\DB::raw("GROUP_CONCAT(user_categories.category_id) as category"))
        ->where('user_categories.user_id',$user->id)
        ->groupBy("user_categories.user_id")
        ->get()
        ->first();

        $SubCategories = UserCategory::select(\DB::raw("GROUP_CONCAT(user_categories.subcategory_id) as subcategory"))
        ->where('user_categories.user_id',$user->id)
        ->groupBy("user_categories.user_id")
        ->get()
        ->first();

        $catCondi = "";
        $subCatCondi = "";
        if($Categories['category']){
            $catCondi = ' AND category_id IN (' . $Categories['category'] . ')';

        }

        if($SubCategories['subcategory']){
            $subCatCondi = ' AND subcategory_id IN (' . $SubCategories['subcategory'] . ')';
        }
        $ads = \DB::select(\DB::raw('SELECT  adverts.id, latitude, longitude, `option`, category_id, subcategory_id, (6371 * ACOS( 
                                    COS( RADIANS(' . $data['latitude'] . ') ) 
                                * COS( RADIANS( latitude ) ) 
                                * COS( RADIANS( longitude ) - RADIANS(' . $data['longitude'] . ') ) 
                                + SIN( RADIANS(' . $data['latitude'] . ') ) 
                                * SIN( RADIANS( latitude ) )
                                    ) ) AS distance FROM `adverts`
                            JOIN reach_options 
                            ON reach_options.id = `adverts`.`reach_id`
                                WHERE reach_options.id IN (1, 2, 3, 6, 7, 8) ' .$catCondi . $subCatCondi. '
                        HAVING distance <= `option`'))
                       ;
                        
        $ids = [];
        foreach($ads as $ad){
            $ids[] = $ad->id;
        }
        $advert = Advert::orderBy("id", "desc")
                        ->where('status',1)
                        ->whereIn('id', $ids)
                        ->paginate(20);
        foreach($advert as $adverts)
        {
            if($adverts->photo)
            {
                $adverts['photo'] = Config::get('constants.image').$adverts->photo;   
            }
            if($adverts->flayer_photo)
            {
                $adverts['flayer_photo'] = Config::get('constants.image').$adverts->flayer_photo; 
            }
        }
        return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Advert Liting....",'data' => $advert]);
       
    }
    public function add(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();
        $list = Advert::where('user_id',$user->id)
                        ->where('status','1')
                        ->where('reach_id','1')
                        ->first();
        if($data['reach'] == 'kenya')
        {
            if($list && isset($data['reach_id'])&& $data['reach_id'] == 1)
            {
                return response()->json(['status'=> false,'statusCode' => '400' ,"message" =>"you are already selected this reach....!",'data' =>null]);
            }
        }
        
        if($data['reach'] == 'usa')
        {
            if($list && isset($data['reach_id'])&& $data['reach_id'] == 6)
            {
                return response()->json(['status'=> false,'statusCode' => '400' ,"message" =>"you are already selected this reach....!",'data' =>null]);
            }
        }
        $validator = Validator::make($request->all(), [ 
            'category_id' => 'required', 
            'subcategory_id' => 'required', 
            'description' => 'required', 
            'price' => 'required', 
            'name' => 'required', 
            'address' => 'required',
            'mobileno'=>'required',
            'reach_id'=>'required',
        ]);
        if ($validator->fails()) { 
            return response()->json(['status'=> false,'ErrorCode' => '400' ,"message" =>"Somthing Wrong Please try again",'data' => $validator->errors()]);
        }
        // if($data['photo'] != null)
        // {
        //     $data['photo'] = $request->file('photo')->hashName();
        //     $request->file('photo')
        //     ->store('image', ['disk' => 'public']);
        //     // $data['url'] = Config::get('constants.image').$data['photo'];
        // }
        // if($data['flayer_photo'] != null)
        // {
        //     $data['flayer_photo'] = $request->file('flayer_photo')->hashName('');
        //     $request->file('flayer_photo')
        //     ->store('image', ['disk' => 'public']);
        //     // $data['url'] = Config::get('constants.image').$data['flayer_photo'];
        // }
        if($data['photo'] != null)
        {
            $folderPath = public_path() ."/images/";

            $image_parts = explode(";base64,", $data['photo']);
            $image_type_aux = explode("images/", $image_parts[0]);
            $image_type = isset($image_type_aux[1]) ? $image_type_aux[1] : "png";
            $image_base64 = base64_decode($image_parts[1]);
            $fileName = uniqid() . '.'.$image_type;
            $file = $folderPath . $fileName;

            file_put_contents($file, $image_base64);
            $data['photo'] = $fileName;
        }
        if($data['flayer_photo'] != null)
        {
            $folderPath = public_path() ."/images/";

            $image_parts = explode(";base64,", $data['flayer_photo']);
            $image_type_aux = explode("images/", $image_parts[0]);
            $image_type = isset($image_type_aux[1]) ? $image_type_aux[1] : "png";
            $image_base64 = base64_decode($image_parts[1]);

            $fileName = uniqid() . '.'.$image_type;
            $file = $folderPath . $fileName;

            file_put_contents($file, $image_base64);
            $data['flayer_photo'] = $fileName;
        }
        $data['user_id'] = $user->id;
        $data['status'] = 1;
        $advert = Advert::create($data);
        $advert->expiry_date = $advert->created_at;
        $advert->save();
        return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Advert Add Successfully....!",'data' =>$advert]);


    }
    public function update(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();
        
        $advert = Advert::where('id',$data['advert_id'])->first();
        if($advert)
        {
            $advert->user_id = $user->id;
            $advert->category_id = $request->input('category_id');
            $advert->subcategory_id = $request->input('subcategory_id');
            $advert->description = $request->input('description');
            $advert->price = $request->input('price');
            $advert->name = $request->input('name');
            $advert->address = $request->input('address');
            $advert->mobileno = $request->input('mobileno');
            $advert->latitude = $request->input('latitude');
            $advert->longitude = $request->input('longitude');
            $advert->reach_id = $request->input('reach_id');
            $advert->photo = $request->input('photo');
            $advert->flayer_photo = $request->input('flayer_photo');
            $advert->country_id = $request->input('country_id');
            $advert->state_id = $request->input('state_id');
            // if($data['photo'] != null)
            // {
            //     $advert->photo = $request->file('photo')->hashName();
            //     $request->file('photo')
            //     ->store('image', ['disk' => 'public']);
            // }
            // if($data['flayer_photo'] != null)
            // {
            //     $advert->flayer_photo = $request->file('flayer_photo')->hashName('');
            //     $request->file('flayer_photo')
            //     ->store('image', ['disk' => 'public']);
            // }
            // if($request->input('photo') != null)
            // {
            //     $folderPath = public_path() ."/images/";

            //     $image_parts = explode(";base64,", $data['photo']);
            //     $image_type_aux = explode("image/", $image_parts[0]);
            //     $image_type = $image_type_aux[1];
            //     $image_base64 = base64_decode($image_parts[1]);
            //     $file = $folderPath . uniqid() . '. '.$image_type;

            //     file_put_contents($file, $image_base64);
            // }
            // if($request->input('flayer_photo') != null)
            // {
            //     $folderPath = public_path() ."/images/";

            //     $image_parts = explode(";base64,", $data['flayer_photo']);
            //     $image_type_aux = explode("image/", $image_parts[0]);
            //     $image_type = $image_type_aux[1];
            //     $image_base64 = base64_decode($image_parts[1]);
            //     $file = $folderPath . uniqid() . '. '.$image_type;

            //     file_put_contents($file, $image_base64);
            // }
            $advert->save();
        }
        
        return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Advert Update Successfully....!",'data' =>$advert]);

    }
    public function search(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();
        $search = $request->get('search');
        
        $advert = Advert::select('adverts.*')
                        ->with('category','subcategory')
                        ->join('categories','categories.id','adverts.category_id')
                        ->join('sub_categories','sub_categories.id','adverts.subcategory_id')
                        ->where('status',1)
                        ->where('description', 'like', "{$search}%")
                        ->orWhere('adverts.name', 'like', "{$search}%")
                        ->orWhere('categories.name', 'like', "{$search}%")
                        ->orWhere('sub_categories.name', 'like', "{$search}%")
                        ->paginate(20);
        
        if(count($advert) > 0)
        {
            foreach($advert as $adverts)
            {
                if($adverts->photo)
                {
                    $adverts['photo'] = Config::get('constants.image').$adverts->photo;   
                }
                if($adverts->flayer_photo)
                {
                    $adverts['flayer_photo'] = Config::get('constants.image').$adverts->flayer_photo; 
                } 
            }
            return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Advert Liting....",'data' => $advert]);
        }
        else{
            return response()->json(['status'=> false,'statusCode' => '400' ,"message" =>"No Data Found....",'data' => null]);
        }
    }
    public function myadvert(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();
        $advert = Advert::where('user_id',$user->id)->get();
        if(count($advert)) 
        {
            foreach($advert as $adverts)
            {
                if($adverts->photo)
                {
                    $adverts['photo'] = Config::get('constants.image').$adverts->photo;   
                }
                if($adverts->flayer_photo)
                {
                    $adverts['flayer_photo'] = Config::get('constants.image').$adverts->flayer_photo; 
                }
            }
            return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Advert Liting....",'data' => $advert]);
        }
        else{
            return response()->json(['status'=> false,'statusCode' => '400' ,"message" =>"No Data Found....",'data' => null]);
        }
    }
    public function advert_update_status(Request $request)
    {
        $data = $request->all();
        $advert = Advert::where('id',$data['advert_id'])->first();
        $advert->status = $request->input('status');
        $advert->save();
        return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Advert Update Successfully....!",'data' =>$advert]);

    }
    public function delete(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();
        $advert = Advert::where('id',$data['advert_id'])->first();
        if($advert)
        {
            $advert->delete();
            return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Advert Delete Successfully....!",'data' =>null]);
        }
        else{
            return response()->json(['status'=> false,'statusCode' => '400' ,"message" =>"No data Found....!",'data' =>null]);
        }
    }
    public function createImage($img)
    {

        $folderPath = public_path() ."images/";

        $image_parts = explode(";base64,", $img);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $file = $folderPath . uniqid() . '. '.$image_type;

        file_put_contents($file, $image_base64);

        return $file;

    }
}
