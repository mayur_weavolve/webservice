<?php
return [
    'app' => [
        'title' => 'General Settings',
        'desc' => 'All the general settings for application.',
        'icon' => 'fas fa-cogs',

        'elements' => [
            [
                'type' => 'text', // input fields type
                'data' => 'string', // data type, string, int, boolean
                'name' => 'app_name', // unique name for field
                'label' => 'App Name', // you know what label it is
                'rules' => 'required|min:2|max:100', // validation rule of laravel
                'class' => 'px-2', // any class for input
                'value' => 'Simba SOS Button', // default value if you want
                'readonly' => true // non-editable
            ],
            [
                'type' => 'textarea', // input fields type
                'data' => 'string', // data type, string, int, boolean
                'name' => 'app_welcome_msg', // unique name for field
                'label' => 'Welcome Message', // you know what label it is
                'rules' => 'required', // validation rule of laravel
                'class' => 'px-2', // any class for input
                'value' => '', // default value if you want
                'rows'  => '5',
            ],
            [
                'type' => 'textarea', // input fields type
                'data' => 'string', // data type, string, int, boolean
                'name' => 'app_marketplace_msg', // unique name for field
                'label' => 'Marketplace Message', // you know what label it is
                'rules' => 'required', // validation rule of laravel
                'class' => 'px-2', // any class for input
                'value' => '', // default value if you want
                'rows'  => '5',
            ]
        ]
    ],
    /*'email' => [

        'title' => 'Email',
        'desc' => 'Email settings for app',
        'icon' => 'glyphicon glyphicon-envelope',

        'elements' => [
            [
                'type' => 'email',
                ...
            ],
            [
                ...
            ],
            [
                ...
            ]
        ]
    ],*/
];
